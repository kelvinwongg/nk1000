<?php
// error_reporting(E_ALL);
// ini_set('display_errors', TRUE);
// ini_set('display_startup_errors', TRUE);
// ini_set('soap.wsdl_cache_enabled',0);
// ini_set('soap.wsdl_cache_ttl',0);

function xd($var) {
  echo '<pre>';
  var_dump($var);
  echo '</pre>';
}

class clsWSSEAuth { 
          private $Username; 
          private $Password;  
        function __construct($username, $password) { 
                 $this->Username=$username; 
                 $this->Password=$password; 
              } 
} 

class clsWSSEToken { 
        private $UsernameToken; 
        function __construct ($innerVal){ 
            $this->UsernameToken = $innerVal; 
        } 
} 

/* Create a class for your webservice structure, in this case: api01Req */
class api01Req {
    function __construct($weight = '1', $countryCode = 'USA') 
    {
      $this->ecshipUsername = 'tkelvinwong';
      // $this->ecshipUsername = 'kelvinwong';
      $this->integratorUsername = 'kelvinwong';
      $this->countryCode = $countryCode;
      $this->shipCode = 'EMS';
      $this->weight = $weight;
    }
}

class api02Req {
    function __construct() 
    {
      $this->ecshipUsername = 'tkelvinwong';
      // $this->ecshipUsername = 'kelvinwong';
      $this->integratorUsername = 'kelvinwong';
      $this->shipCode = 'EMS';
      $this->countryCode = 'USA';
      $this->mailType = '';
      $this->insurAmount = 50000;
      $this->insurTypeCode = '0';
      $this->satchelTypeCode = 'S0';
      $this->itemCategory = 'G';
      $this->senderCountry = 'Hong Kong SAR';
      $this->senderName = 'Chan Tai Man Sender';
      $this->senderAddress = 'This is the address Sender';
      $this->senderEmail = 'sender@email.com';
      
      $data = new \stdClass();
      $data->productQty = 1;
      $data->productValue = 100;
      $data->productValue = 2;
      $data->currencyCode = 'HKD';
      $data->productWeight = 1;
      $data->contentDesc = 'Toy';
      $products = array();
      $products[] = new SoapVar( $data, XSD_ANYTYPE, null, null, 'Product' );
      // xd($products);
      $this->products = $products;
      
      $this->recipientName = 'Law Mei Mei Recipient';
      $this->recipientAddress = 'This is the address Recipient';
      $this->recipientCity = 'City Recipient';
      $this->recipientPostalNo = '99999';
    }
}

class api11Req {
    function __construct() 
    {
      $this->ecshipUsername = 'tkelvinwong';
  		// $this->ecshipUsername = 'kelvinwong';
      $this->integratorUsername = 'kelvinwong';
      $this->printMode = '0';
      $itemNos[] = new SoapVar( 'LL999532611HK', XSD_STRING, null, null, 'element' );
      // xd($itemNos);
      $this->itemNo = $itemNos;
    }
}

// Creating date using yyyy-mm-ddThh:mm:ssZ format
$tm_created = gmdate('Y-m-d\TH:i:s\Z');
$tm_expires = gmdate('Y-m-d\TH:i:s\Z', gmdate('U') + 180);


// Generating, packing and encoding a random number
$simple_nonce = mt_rand();
$encoded_nonce = base64_encode(pack('H*', $simple_nonce));


$username = "kelvinwong"; 
// $password = "3535dbff-8835-49c1-bd1f-7030137a447d";
$password = "3535dbff-8835-49c1-bd1f-7030137a447d";
$passdigest = base64_encode(pack('H*',sha1(pack('H*', $simple_nonce) . pack('a*', $tm_created) . pack('a*', $password))));


// Initializing namespaces
$ns_wsse = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$ns_wsu = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd';
$password_type = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest';
$encoding_type = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary';

// Creating WSS identification header using SimpleXML
$root = new SimpleXMLElement('<root/>');

$security = $root->addChild('wsse:Security', null, $ns_wsse);

$usernameToken = $security->addChild('wsse:UsernameToken', null, $ns_wsse);
$usernameToken->addChild('wsse:Username', $username, $ns_wsse);
$usernameToken->addChild('wsse:Password', $passdigest, $ns_wsse)->addAttribute('Type', $password_type);
$usernameToken->addChild('wsse:Nonce', $encoded_nonce, $ns_wsse)->addAttribute('EncodingType', $encoding_type);
$usernameToken->addChild('wsu:Created', $tm_created, $ns_wsu);

// Recovering XML value from that object
$root->registerXPathNamespace('wsse', $ns_wsse);
$full = $root->xpath('/root/wsse:Security');
$auth = $full[0]->asXML();

$objSoapVarWSSEHeader = new SoapHeader($ns_wsse, 'Security', new SoapVar($auth, XSD_ANYXML), true);

// TEST
$objClient = new SoapClient("https://service.hongkongpost.hk/API-trial/services/Calculator?wsdl");
// $objClient = new SoapClient("https://service.hongkongpost.hk/API-trial/services/Posting?wsdl", array('trace' => 1));

// LIVE
// $objClient = new SoapClient("https://api.hongkongpost.hk/API/services/Calculator?wsdl");
// $objClient = new SoapClient("https://api.hongkongpost.hk/API/services/Posting?wsdl", array('trace' => 1));

$objClient->__setSoapHeaders(array($objSoapVarWSSEHeader));

$api01Req = new api01Req('2', 'CNA');
xd($api01Req);

// $api02Req = new api02Req();
// xd($api02Req);

// $api11Req = new api11Req();
// xd($api11Req);

$params = array(
  "api01Req" => $api01Req
  // "api02Req" => $api02Req
  // "api11Req" => $api11Req
);

$objResponse = $objClient->__soapCall("getTotalPostage",array($params));
// $objResponse = $objClient->__soapCall("createOrder",array($params));
// $objResponse = $objClient->__soapCall("getAddressPack",array($params));

// Dump SOAP request
// echo 'SOAP Request:<br>';
// print_r( $objClient->__getLastRequestHeaders() );
// print_r( htmlentities($objClient->__getLastRequest()) );

/* Print webservice response */
xd($objResponse);

// file_put_contents('./getAddressPack.pdf', $objResponse->getAddressPackReturn->ap);
// header("Content-type: application/pdf");
// header("Content-Disposition: inline; filename=filename.pdf");
// @readfile('./getAddressPack.pdf');

// object(stdClass)#11 (1) {
//   ["createOrderReturn"]=>
//   object(stdClass)#12 (7) {
//     ["errMessage"]=>
//     string(7) "Success"
//     ["status"]=>
//     int(0)
//     ["additionalDocument"]=>
//     string(6) "1C0D0G"
//     ["deliveryCharge"]=>
//     string(6) "292.00"
//     ["insurPermFee"]=>
//     string(6) "203.00"
//     ["itemNo"]=>
//     string(13) "LL999532611HK"
//     ["orderNo"]=>
//     string(16) "P000000001158146"
//   }
// }
