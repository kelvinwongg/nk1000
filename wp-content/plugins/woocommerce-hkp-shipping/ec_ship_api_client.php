<?php
// error_reporting(E_ALL);
// ini_set('display_errors', TRUE);
// ini_set('display_startup_errors', TRUE);

// function xd($var) {
//   echo '<pre>';
//   var_dump($var);
//   echo '</pre>';
// }

class clsWSSEAuth { 
          private $Username; 
          private $Password;  
        function __construct($username, $password) { 
                 $this->Username=$username; 
                 $this->Password=$password; 
              } 
} 

class clsWSSEToken { 
        private $UsernameToken; 
        function __construct ($innerVal){ 
            $this->UsernameToken = $innerVal; 
        } 
} 

/* Create a class for your webservice structure, in this case: api01Req */
class api01Req {
    function __construct($weight = '1', $countryCode = 'USA') 
    {
            $this->ecshipUsername = 'tkelvinwong';
            $this->integratorUsername = 'kelvinwong';
            $this->countryCode = $countryCode;
            $this->shipCode= 'EMS';
            $this->weight= $weight;
    }
}

class HKP_EC_SHIP_CLIENT {
  
  // private $tm_created, $tm_expires, $simple_nonce, $encoded_nonce, $username, $password, $passdigest, $ns_wsse, $ns_wsu, $password_type, $encoding_type, $root, $security, $usernameToken, $full, $auth, $objSoapVarWSSEHeader, $objClient;
  
  public static function get_postage_charge($weight, $countryCode) {
    // Creating date using yyyy-mm-ddThh:mm:ssZ format
    $tm_created = gmdate('Y-m-d\TH:i:s\Z');
    $tm_expires = gmdate('Y-m-d\TH:i:s\Z', gmdate('U') + 180);


    // Generating, packing and encoding a random number
    $simple_nonce = mt_rand();
    $encoded_nonce = base64_encode(pack('H*', $simple_nonce));


    $username = "kelvinwong"; 
    $password = "3535dbff-8835-49c1-bd1f-7030137a447d"; 
    $passdigest = base64_encode(pack('H*',sha1(pack('H*', $simple_nonce) . pack('a*', $tm_created) . pack('a*', $password))));


    // Initializing namespaces
    $ns_wsse = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
    $ns_wsu = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd';
    $password_type = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest';
    $encoding_type = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary';

    // Creating WSS identification header using SimpleXML
    $root = new SimpleXMLElement('<root/>');

    $security = $root->addChild('wsse:Security', null, $ns_wsse);

    $usernameToken = $security->addChild('wsse:UsernameToken', null, $ns_wsse);
    $usernameToken->addChild('wsse:Username', $username, $ns_wsse);
    $usernameToken->addChild('wsse:Password', $passdigest, $ns_wsse)->addAttribute('Type', $password_type);
    $usernameToken->addChild('wsse:Nonce', $encoded_nonce, $ns_wsse)->addAttribute('EncodingType', $encoding_type);
    $usernameToken->addChild('wsu:Created', $tm_created, $ns_wsu);

    // Recovering XML value from that object
    $root->registerXPathNamespace('wsse', $ns_wsse);
    $full = $root->xpath('/root/wsse:Security');
    $auth = $full[0]->asXML();

    $objSoapVarWSSEHeader = new SoapHeader($ns_wsse, 'Security', new SoapVar($auth, XSD_ANYXML), true);

    try {
      $objClient = new SoapClient("https://service.hongkongpost.hk/API-trial/services/Calculator?wsdl");
      $objClient->__setSoapHeaders(array($objSoapVarWSSEHeader));
      
      // $api01Req = new api01Req('2', 'USA');
      // $api01Req = new api01Req('2', 'CN');
      // $api01Req = new api01Req('2', 'CNG');
      // $api01Req = new api01Req('2', 'CNA');
      $api01Req = new api01Req($weight, $countryCode);
      // xd($api01Req);
      $params = array(
        "api01Req" => $api01Req
      );

      $objResponse = $objClient->__soapCall("getTotalPostage",array($params));
      if ($objResponse->getTotalPostageReturn->status != 0) {
        xd($objResponse->getTotalPostageReturn->errMessage);
        return false;
      }
      /* Print webservice response */
      // xd($objResponse);
      return $objResponse->getTotalPostageReturn->totalPostage;

    } catch (Exception $e) {
      xd($e->getMessage());
      return false;
    }

    return false;

  }
}
