<?php

/**
 * Plugin Name: WooCommerce HKP SpeedPost Shipping
 * Plugin URI: http://code.tutsplus.com/tutorials/create-a-custom-shipping-method-for-woocommerce--cms-26098
 * Description: WooCommerce Shipping Method for Hong Kong Post SpeedPost (EMS)
 * Version: 0.0.1
 * Author: Kelvin Wong
 * Author URI: http://kelvinwong.hk
 * License: GPL-3.0+
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Domain Path: /lang
 * Text Domain: wc-hkp-shipping
 */

// Die for illegal direct access
if ( ! defined( 'WPINC' ) ) { die; }

/*
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

  include_once( dirname(__FILE__) . '/ec_ship_api_client.php' );
  // include_once( dirname(__FILE__) . '/easypost-php-master/lib/easypost.php' );
  include_once( dirname(__FILE__) . '/kia_convert_country_code.php' );

  function register_wc_hkp_shipping_method() {
    if ( ! class_exists( 'WC_HKP_Shipping_Method' ) ) {
      class WC_HKP_Shipping_Method extends WC_Shipping_Method {
        /**
         * Constructor for your shipping class
         *
         * @access public
         * @return void
         */
        public function __construct( $instance_id = 0 ) {
          $this->id                 = 'wc_hkp_shipping_id'; // Id for your shipping method. Should be uunique.
          $this->method_title       = __( 'HKP SpeedPost Shipping', 'wc-hkp-shipping' ); // Title shown in admin
          $this->method_description = __( 'Shipping Method for Hong Kong Post SpeedPost (EMS)', 'wc-hkp-shipping' ); // Description shown in admin

          // $this->enabled = isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : 'yes';
          $this->enabled = 'no';
          $this->title = isset( $this->settings['title'] ) ? $this->settings['title'] : __( 'HKP SpeedPost Shipping', 'wc-hkp-shipping' );
            
          $this->instance_id = absint( $instance_id );

          $this->supports  = array(
            'shipping-zones',
            'settings',
            'instance-settings',
            'instance-settings-modal',
          );

          // Availability & Countries
          // $this->availability = 'including';
          // $this->countries = array(
          //     'US', // Unites States of America
          //     'CA', // Canada
          //     'DE', // Germany
          //     'GB', // United Kingdom
          //     'IT', // Italy
          //     'ES', // Spain
          //     'HR' // Croatia
          // );

          $this->init();
        }

        /**
         * Init your settings
         *
         * @access public
         * @return void
         */
        function init() {
          // Load the settings API
          $this->init_form_fields();
          $this->init_settings();

          // Save settings in admin if you have any defined
          add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
        }

        /**
         * Define settings field for this shipping
         * @return void 
         */
        function init_form_fields() { 

          $this->form_fields = array(

            'enabled' => array(
              'title' => __( 'Enable', 'wc-hkp-shipping' ),
              'type' => 'checkbox',
              'description' => __( 'Enable this shipping.', 'wc-hkp-shipping' ),
              'default' => 'yes'
            ),

            'title' => array(
              'title' => __( 'Title', 'wc-hkp-shipping' ),
              'type' => 'text',
              'description' => __( 'Title to be display on site', 'wc-hkp-shipping' ),
              'default' => __( 'HKP SpeedPost Shipping', 'wc-hkp-shipping' )
            ),

         );

        }

        /**
         * This function is used to calculate the shipping cost. Within this function we can check for weights, dimensions and other parameters.
         *
         * @access public
         * @param mixed $package
         * @return void
         */
        public function calculate_shipping( $package = array() ) {
          // xd($package);

          $weight = 0;
          $cost = 0;
          $country = $package["destination"]["country"];
          // xd($country);
          // xd(kia_convert_country_code($country));

          foreach ( $package['contents'] as $item_id => $values ) { 
            $_product = $values['data']; 
            $weight = $weight + $_product->get_weight() * $values['quantity']; 
          }

          $weight = (string)wc_get_weight( $weight, 'kg' );
          // echo '<pre>' . $weight . 'kg</pre>';

          // $cost = HKP_EC_SHIP_CLIENT::get_postage_charge($weight, kia_convert_country_code($country));
          // echo '<pre>HKD$' . $cost . '</pre>';
          
          // Query endpoint at https://www.nk1000.com/postageCalculator.php?weight=&country=
          $json = file_get_contents('https://www.nk1000.com/postageCalculator.php?weight=' . $weight . '&country=' . kia_convert_country_code($country));
          $obj = json_decode($json);
          // xd($obj->response->getTotalPostageReturn->totalPostage);
          $cost = $obj->response->getTotalPostageReturn->totalPostage;

          // Register the rate
          $rate = array(
            'id' => $this->id,
            'label' => $this->title,
            'cost' => $cost
          );
          $this->add_rate( $rate );

        }

      }
    }
  }

  add_action( 'woocommerce_shipping_init', 'register_wc_hkp_shipping_method' );

  function add_wc_hkp_shipping_method( $methods ) {
    $methods['wc_hkp_shipping_id'] = 'WC_HKP_Shipping_Method';
    return $methods;
  }

  add_filter( 'woocommerce_shipping_methods', 'add_wc_hkp_shipping_method' );

}