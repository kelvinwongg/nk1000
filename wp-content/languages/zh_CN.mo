��    Z      �     �      �     �  
   �     �  	   �     �     �     �     �       
     
        $     5  
   <     G     P     o     u     z     �     �     �     �  	   �     �     �     �  
   �     �     �     	     	     	  (   #	     L	  F   S	     �	     �	     �	     �	     �	  E   �	  !   
  )   9
  5   c
     �
  <   �
     �
     �
     �
  6   �
  4     -   T     �  +   �     �     �  	   �     �     �     �     �     �     �                     /  	   7     A  $   J     o     v  
   {  F   �  %   �  $   �  	        "  4   (  !   ]          �     �     �     �  +   �     �     �  �  �  	   �     �     �     �     �     �     �     �               (     /     <     C     P     V     o     v     }     �     �     �     �  	   �     �     �     �     �     �     �                     '     H  E   O     �     �     �     �     �  /   �      �  '     '   D     l  8   s     �     �     �  -   �  !   �  $     	   3     =     \  	   c  	   m     w     ~     �     �     �  +   �     �     �     �     �     �     �          -     1     8  3   E     y     �  	   �     �  3   �     �       	        #     ?     N  '   [     �     �        O         D                   K   F   !   $   >      E   W      <   Z           Y   @       ,      0       "   =       	   2   X   5   )               P   ;       1   +       4          R      9   U           &           C   #          *   M                   -       ?   J   N                        G       6   S         B      3   %   A          :   
   Q       /   (   L                       8   V   7   I       T               '             H                  .       %1$s at %2$s (no title) ,  1 Comment Action Activate Add Add or remove tags Advanced Author: %s Background Background color Cancel Categories Checkout Choose from the most used tags Clear Code Comments Continue reading %s Copy Current page of the collection. Custom Link Dashboard Date Default Description Dimensions Dismiss Dismiss this notice. Done Edit Email Ensure result set excludes specific IDs. Header If this was a mistake, just ignore this email and nothing will happen. Image Images Invalid post ID. Leave a Reply to %s Left Limit result set to all items except those of a particular parent ID. Limit result set to specific IDs. Limit results to those matching a string. Maximum number of items to be returned in result set. Menu Method '%s' not implemented. Must be overridden in subclass. Name Next No No route was found matching the URL and request method Offset the result set by a specific number of items. Order sort attribute ascending or descending. Pages: Password reset is not allowed for this user Preview Previous Published Remove Remove image Right Search Select Separate tags with commas Settings Show Show hierarchy Sidebar Site URL. Sort by: Sort collection by object attribute. Source Tags Text color The changes you made will be lost if you navigate away from this page. The comment has already been trashed. The handler for the route is invalid Thumbnail Title To reset your password, visit the following address: Unique identifier for the object. Update Username Username isn't editable. Username: %s We Recommend Whether to bypass trash and force deletion. Year Yes Project-Id-Version: WordPress - 4.9.x - Development
POT-Creation-Date: 2018-08-16 00:49+0800
PO-Revision-Date: 2018-08-16 00:50+0800
Last-Translator: 
Language-Team: 
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 2.1.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: themes
X-Poedit-SearchPath-1: plugins/woocommerce
 %1$s %2$s (无标题) 、 一条评论 操作 启用 添加 添加或删除标签 高级 作者：%s 背景 背景颜色 取消 分类目录 XXsdf 从常用标签中选择 清空 代码 评论 继续阅读%s 复制 集合的当前页。 自定义链接 仪表盘 日期 默认 图像描述 尺寸 不再显示 忽略此通知。 完成 编辑 电子邮件 确保结果集排除指定ID。 顶部 若这不是您本人要求的，请忽略本邮件，一切如常。 图像 图像 文章ID无效。 向%s进行回复 左 限制结果集为没有特定父ID的文章。 将结果集限制到指定ID。 将结果限制为匹配字符串的。 结果集包含的最大项目数量。 菜单 方法‘%s’未实现，必须在子类中被覆盖。 姓名 继续 否 未找到匹配URL和请求方式的路由。 将结果集移位特定数量。 设置排序字段升序或降序。 页面： 不能重设该用户的密码 预览 上一个 已发布 移除 移除图像 右 搜索 选择 多个标签请用英文逗号（,）分开 设置 显示 体现层级关系 边栏 站点URL。 排序依据： 按对象属性排序集合。 源 标签 文字颜色 离开这个页面，您所做的更改将丢失。 此评论已在回收站。 此路由的句柄无效。 缩略图 标题 要重置您的密码，请打开下面的链接： 对象的唯一标识符。 更新 用户名 用户名不能被编辑。 用户名：%s 推荐产品 是否绕过回收站并强行删除。 年 是 