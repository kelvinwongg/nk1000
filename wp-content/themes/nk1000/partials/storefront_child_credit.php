<div class="site-info">
  <div class="col-full">
    <div class="grid-x">
      <div class="cell small-12 medium-6 links">
        <a href="<?php echo get_home_url(); ?>/terms-of-use/"><?php _e("Terms of use"); ?></a>
        |
        <a href="<?php echo get_home_url(); ?>/privacy-policy/"><?php _e("Privacy Policy"); ?></a>
      </div>
      <div class="cell small-12 medium-6 copyright">
        <?php printf( __( '© %s by Nippon Kendai All rights reserved.', 'storefront_child' ), date("Y") ); ?>
      </div>
    </div>
  </div>
</div><!-- .site-info -->