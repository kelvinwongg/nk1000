<?php
// Partial arguments
$post_id = $args['post_id'];
?>
<section class="storefront-product-section storefront-child-page-banner" aria-label="Storefront Child Page Banner">
  <?php echo wp_get_attachment_image( get_post_thumbnail_id( $post_id ), 'full' ); ?>
</section>