<?php
// Partial arguments
$hero_slider = $args['hero_slider'];

// xd($hero_slider);
?>
<section class="storefront-product-section storefront-child-product-hero-slider" aria-label="Storefront Child Hero Slider">
  <div class="slider">
    <?php foreach ($hero_slider as $this_slide) : ?>
      <?php //xd($this_slide); ?>
      <div class="slide">
        <a href="<?php echo $this_slide['url']; ?>">
          <div class="grid-x align-middle">
            <div class="cell small-12 medium-8 slide-image"><?php echo wp_get_attachment_image( $this_slide['image']['ID'], 'large' ); ?></div>
            <div class="cell small-12 medium-4 slide-text">
              <h2><?php echo $this_slide['title']; ?></h2>
              <?php echo $this_slide['text']; ?>
            </div>
          </div>
        </a>
      </div>
    <?php endforeach; ?>
  </div>
</section>