<?php
/**
 * The template for displaying full width pages.
 *
 * Template Name: Contact Us
 *
 * @package storefront child
 */

get_header(); ?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

      <div class="grid-x grid-margin-x">
        <div class="cell small-12 medium-6">
          <?php while ( have_posts() ) : the_post();

            do_action( 'storefront_page_before' );

            get_template_part( 'content', 'page' );

            /**
             * Functions hooked in to storefront_page_after action
             *
             * @hooked storefront_display_comments - 10
             */
            do_action( 'storefront_page_after' );

          endwhile; // End of the loop. ?>
        </div>
        <div class="cell small-12 medium-6">
          <?php
          switch (ICL_LANGUAGE_CODE) {
            case 'en':
              echo do_shortcode('[ninja_form id=1]');
              break;
            case 'tc':
              echo do_shortcode('[ninja_form id=2]');
              break;
            case 'sc':
              echo do_shortcode('[ninja_form id=3]');
              break;
            
            default:
              echo do_shortcode('[ninja_form id=1]');
              break;
          }
          ?>
        </div>
      </div>

    </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_footer();