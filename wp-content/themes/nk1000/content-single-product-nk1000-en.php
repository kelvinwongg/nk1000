<?php //xd(get_field('use_mikei_template')); ?>
<style>
#content.site-content > .col-full {
  background-color: #ddd;
  padding: 0;
  margin: 0;
  max-width: 5000px;
}
#content.site-content > .col-full > .woocommerce {
  max-width: 66.4989378333em;
  margin-left: auto;
  margin-right: auto;
  padding: 0 2.617924em;
  box-sizing: content-box;
}

/* Foundation 6 Media Queries START */
/* Small only */
@media screen and (max-width: 39.9375em) {}
/* Medium and up */
@media screen and (min-width: 40em) {}
/* Medium only */
@media screen and (min-width: 40em) and (max-width: 63.9375em) {}
/* Large and up */
@media screen and (min-width: 64em) {}
/* Large only */
@media screen and (min-width: 64em) and (max-width: 74.9375em) {}
/* Foundation 6 Media Queries END */

/*GENERAL START*/
/*.text-center { text-align: center; }
.text-right { text-align: right; }*/
img { display: inline-block; }
.font-serif { font-family: serif; }
.font-black { color: black; }
.font-white { color: white; }
.font-gold { color: rgb(124, 46, 6); }
.font-bold { font-weight: bold; }
.font-wider { letter-spacing: .2em; }

.bg-white { background-color: white; }
.bg-red-lighter { background-color: rgb(153,44,3); }
.bg-red-darker { background-color: rgb(92,26,0); }
.bg-yellow-pale { background-color: rgb(247,240,214); }

.margin-0 { margin: 0; }
.margin-top-1em { margin-top: 1em; }
.margin-bottom-1em { margin-bottom: 1em; }
.margin-top-2em { margin-top: 2em; }
.margin-bottom-2em { margin-bottom: 2em; }

.padding-0 { padding: 0; }
.padding-top-4em { padding-top: 4em; }
.padding-bottom-4em { padding-bottom: 4em; }
.padding-top-5em { padding-top: 5em; }
.padding-bottom-5em { padding-bottom: 5em; }
.padding-top-bottom-2em { padding-top: 2em; padding-bottom: 2em; }
/*GENERAL END*/

/*START: SECTION HERO*/
.section.hero {
  /*height: 1200px;*/
  background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/HIGHRES_banner_web.jpg');
  background-repeat: no-repeat;
  background-position: center top;
  padding-top: 4em;
  background-color: rgb(248, 228, 209);
}
.section.hero .product-tagline h1 { margin-bottom: .3em; }
.section.hero .product-tagline-2nd { font-size: 1.5em; }
.section.hero .features-highlights { padding-top: 2em; }
.section.hero .features-highlights > .grid-x > .cell { position: relative; }
.section.hero .features-highlights > .grid-x > .cell .overlay {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgb(0,0,0);
  background: -webkit-linear-gradient(bottom, rgba(0,0,0,1) 0%, rgba(255,255,255,0) 38%, rgba(255,255,255,0) 100%);
  background: -o-linear-gradient(bottom, rgba(0,0,0,1) 0%, rgba(255,255,255,0) 38%, rgba(255,255,255,0) 100%);
  background: linear-gradient(to top, rgba(0,0,0,1) 0%, rgba(255,255,255,0) 38%, rgba(255,255,255,0) 100%);
}
.section.hero .features-highlights > .grid-x > .cell .text {
  padding: 2em;
  position: absolute;
  bottom: 0;
  width: 100%;
  font-size: 1.5em;
  color: white;
}
.section.hero .ta {
  padding: 3em 0em;
}
/* Large and up */
@media screen and (min-width: 64em) {
  .section.hero .ta {
    padding: 3em 5em;
  }
}
.section.hero .ta .title {
  font-size: 1.5em;
  font-weight: bold;
  margin-bottom: 2em;
}
.section.hero .ta .text {
  padding: 1em;
  background-color: white;
  font-size: .9em;
  /*letter-spacing: .3em;*/
}
/* Large and up */
@media screen and (min-width: 64em) {
  .section.hero .ta .text {
    font-size: 1.2em;
  }
}
/*END: SECTION HERO*/

/*START: SECTION PURCHASE*/
.section.purchase {
  padding-top: 4em;
  padding-bottom: 4em;
  background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/npk-redresi-pdtpage-v6-07.png');
  background-repeat: no-repeat;
  background-position: center top;
  background-size: cover;
}
.section.purchase .add-to-cart-button .quantity {
  display: inline-block;
  margin-right: 10px;
  vertical-align: top;
}
.section.purchase .add-to-cart-button .quantity input {
  margin-bottom: 0;
}
.section.purchase .add-to-cart-button button {
  margin-bottom: 0;
  height: 40px;
  background-color: rgb(90, 90, 90);
  color: white;
}
/* Large and up */
@media screen and (min-width: 64em) {
  .section.purchase img {
    /*width: 100%;*/
  }
}
/*END: SECTION PURCHASE*/

/*START: SECTION PRODUCT-SPEC*/
.section.product-spec {}
.section.product-spec .product-spec-buttons .accordion-trigger .overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: rgba(0,0,0);
    opacity: .2;
    transition: 0.4s;
}
.section.product-spec .product-spec-buttons .accordion-trigger.active .overlay,
.section.product-spec .product-spec-buttons .accordion-trigger:hover .overlay {
    opacity: 0;
}
.section.product-spec .product-spec-expandable {}
.section.product-spec .product-spec-expandable .product-spec-content {
    /*padding: 0 18px;*/
    /*background-color: white;*/
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
}
.section.product-spec .product-spec-expandable .product-spec-content p {
  max-width: 900px;
  margin: 0 auto;
}
.section.product-spec .product-spec-buttons .button {
  font-size: 2em;
  border-radius: 12px;
  box-shadow: 0 0 13px 4px #555;
  /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#c59b6b+0,e4ce95+25,c59b6b+50,e4ce95+75,c59b6b+100 */
  background: rgb(197,155,107); /* Old browsers */
  background: -moz-linear-gradient(45deg, rgba(197,155,107,1) 0%, rgba(228,206,149,1) 25%, rgba(197,155,107,1) 50%, rgba(228,206,149,1) 75%, rgba(197,155,107,1) 100%); /* FF3.6-15 */
  background: -webkit-linear-gradient(45deg, rgba(197,155,107,1) 0%,rgba(228,206,149,1) 25%,rgba(197,155,107,1) 50%,rgba(228,206,149,1) 75%,rgba(197,155,107,1) 100%); /* Chrome10-25,Safari5.1-6 */
  background: linear-gradient(45deg, rgba(197,155,107,1) 0%,rgba(228,206,149,1) 25%,rgba(197,155,107,1) 50%,rgba(228,206,149,1) 75%,rgba(197,155,107,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c59b6b', endColorstr='#c59b6b',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}
.section.product-spec .product-spec-buttons .text {
  text-shadow: 2px 2px 1px #000000;
  font-size: 1.3em;
}
/*DESKTOP START*/
.section.product-spec .product-spec-buttons .spec-1,
.section.product-spec .product-spec-buttons .spec-2,
.section.product-spec .product-spec-buttons .spec-3 {
  padding-top: 4em;
  padding-bottom: 4em;
  position: relative;
  background-size: auto 100%;
  background-size: cover;
  background-position: right center;
  cursor: pointer;
}
.section.product-spec .product-spec-buttons .spec-1 {
  background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-908633070.jpg');
}
.section.product-spec .product-spec-buttons .spec-2 {
  background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-647577514.jpg');
}
.section.product-spec .product-spec-buttons .spec-3 {
  background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-906813516.jpg');
}
.section.product-spec .product-spec-buttons .spec-1 .content,
.section.product-spec .product-spec-buttons .spec-2 .content,
.section.product-spec .product-spec-buttons .spec-3 .content {
  max-width: 19em;
  color: white;
}
.section.product-spec .product-spec-buttons .spec-1 .content { margin: 0 auto; }
.section.product-spec .product-spec-buttons .spec-2 .content { margin: 0 auto; }
.section.product-spec .product-spec-buttons .spec-3 .content { margin: 0 auto; }
/* Large and up */
@media screen and (min-width: 64em) {
  .section.product-spec .product-spec-buttons .spec-1 .content { float: right; margin-right: 4em; }
  .section.product-spec .product-spec-buttons .spec-3 .content { float: left;  margin-left: 4em; }
}
/*DESKTOP END*/
/*END: SECTION PRODUCT-SPEC*/

/*START: SECTION FAQ*/
.section.faq .accordion {
  background-color: transparent;
}
.section.faq .accordion .accordion-title {
  border: 0;
  border-bottom: 1px solid brown;
}
.section.faq .accordion .accordion-title::before {
  color: brown;
  margin-top: -1rem;
  font-size: 3em;
}
.section.faq .accordion .accordion-content {
  background-color: transparent;
}
/*END: SECTION FAQ*/
</style>

<div class="section hero">
  <div class="col-full">
    <div class="grid-x align-middle">
      <div class="small-12 large-5 cell">
        <div class="product-shot text-center">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/product-with-reflection-en.png" alt="PRODUCT%20SHOT">
        </div>
      </div>
      <div class="small-12 large-7 cell text-center">
        <div class="product-logo">
          <div class="grid-x">
            <div class="small-6 small-offset-3 cell">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Mikei-logo_horizontal.png" alt="">
            </div>
          </div>
        </div>
        <div class="product-tagline">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/npk-redresi-pdtpage-v6-19.png" alt="">
        </div>
        <div class="product-tagline-2nd">
          <h4 class="font-black font-serif"><strong>Natural&nbsp;&nbsp;&nbsp;&nbsp;Pure&nbsp;&nbsp;&nbsp;&nbsp;Essence</strong></h4>
          <!-- <div>100% 日本製造</div> -->
        </div>
        <div class="buy-icon">
          <div class="text-center large-text-right">
            <a href="#buy-here" data-scrollTo="#buy-here" class="click-to-scroll"><img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/npk-redresi-pdtpage-v6-25.png" alt=""></a>
          </div>
        </div>
      </div>
    </div>
    <div class="features-highlights">
      <div class="grid-x">
        <div class="small-12 medium-4 cell margin-bottom-1em">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-885287786.jpg" alt="">
          <div class="overlay">
            <div class="text text-center font-wider font-serif">Strengthen immune system</div>
          </div>
        </div>
        <div class="small-12 medium-4 cell margin-bottom-1em">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-625209642.jpg" alt="">
          <div class="overlay">
            <div class="text text-center font-wider font-serif">Boost liver detoxification</div>
          </div>
        </div>
        <div class="small-12 medium-4 cell margin-bottom-1em">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-670284230.jpg" alt="">
          <div class="overlay">
            <div class="text text-center font-wider font-serif">Anti-aging and Anti-oxidation</div>
          </div>
        </div>
      </div>
    </div>
    <div class="ta">
      <div class="grid-x grid-margin-x">
        <div class="small-12 cell">
          <div class="title text-center font-wider font-serif font-black font-bold">Who Needs Red Reishi the Most?</div>
        </div>
        <div class="small-6 large-3 cell bg-white margin-bottom-1em">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-521815364.jpg" alt="">
          <div class="text text-center font-gold font-serif">Face paleness</div>
        </div>
        <div class="small-6 large-3 cell bg-white margin-bottom-1em">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-947431848.jpg" alt="">
          <div class="text text-center font-gold font-serif">Easily getting sick and tired</div>
        </div>
        <div class="small-6 large-3 cell bg-white margin-bottom-1em">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-493155910.jpg" alt="">
          <div class="text text-center font-gold font-serif">Insomnia</div>
        </div>
        <div class="small-6 large-3 cell bg-white margin-bottom-1em">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-663842896.jpg" alt="">
          <div class="text text-center font-gold font-serif">Poor liver condition</div>
        </div>
      </div>
    </div>
  </div>
</div> <!-- .section.hero -->

<div class="section purchase" id="buy-here">
  <div class="col-full">
    <div class="grid-x grid-margin-x align-middle">
      <div class="small-12 medium-4 cell"><img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/MikeiBox_wo_japansticker_tilt.png" alt=""></div>
      <div class="small-12 medium-8 cell">
        <div class="title margin-top-1em">
          <h1 class="font-serif font-gold font-bold">Mikei<sup>®</sup> Red Reishi</h1>
        </div>
        <p class="font-gold">Each Mikei Red Reishi Essence capsule contains 200mg of highly concentrated red reishi extract.<br>
          (The essence concentration ratio is 16.6:1 , each capsule is equivalent to the extract of 3,320mg of superior-grade mature red reishi)<br>
          NPcaps capsule is 50 mg/cap. ( Capsules used are all-natural and suitable for a variety of dietary requirement including  vegetarians, diabetics, and those on restricted diets. ) </p>
        <p class="font-gold margin-0">Capacity: 250mg/cap (60caps/box)</p>
        <p class="font-gold">Weight: 236g</p>
        <h2 style="margin-top: 0;" class="font-gold">HKD: $1,180 for two Packs (Free 20 caps)</h2>
        <p class="font-gold">Suggested retail price: $820 per Pack</p>
        <p class="font-gold margin-0">Directions:Take on an empty stomach with lukewarm water</p>
        <p class="font-gold margin-0">For health maintenance: 1-2 caps daily</p>
        <p class="font-gold">For treatment: 2-4 caps daily</p>
        <div class="add-to-cart-button text-right margin-top-2em">
          <?php
          /**
           * Custom Loop Add to Cart.
           *
           * Template with quantity and ajax.
           */

          if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly.

          global $product;
          ?>

          <?php if ( ! $product->is_in_stock() ) : ?>

           <a href="<?php echo apply_filters( 'out_of_stock_add_to_cart_url', get_permalink( $product->get_id() ) ); ?>" class="button"><?php echo apply_filters( 'out_of_stock_add_to_cart_text', __( 'Read More', 'woocommerce' ) ); ?></a>

          <?php else : ?>

           <?php
           $link = array(
           'url' => '',
           'label' => '',
           'class' => ''
           );

           switch ( $product->get_type() ) {
           case "variable" :
           $link['url'] = apply_filters( 'variable_add_to_cart_url', get_permalink( $product->get_id() ) );
           $link['label'] = apply_filters( 'variable_add_to_cart_text', __( 'Select options', 'woocommerce' ) );
           break;
           case "grouped" :
           $link['url'] = apply_filters( 'grouped_add_to_cart_url', get_permalink( $product->get_id() ) );
           $link['label'] = apply_filters( 'grouped_add_to_cart_text', __( 'View options', 'woocommerce' ) );
           break;
           case "external" :
           $link['url'] = apply_filters( 'external_add_to_cart_url', get_permalink( $product->get_id() ) );
           $link['label'] = apply_filters( 'external_add_to_cart_text', __( 'Read More', 'woocommerce' ) );
           break;
           default :
           if ( $product->is_purchasable() ) {
           $link['url'] = apply_filters( 'add_to_cart_url', esc_url( $product->add_to_cart_url() ) );
           $link['label'] = apply_filters( 'add_to_cart_text', __( 'Add to cart', 'woocommerce' ) );
           $link['class'] = apply_filters( 'add_to_cart_class', 'add_to_cart_button' );
           } else {
           $link['url'] = apply_filters( 'not_purchasable_url', get_permalink( $product->get_id() ) );
           $link['label'] = apply_filters( 'not_purchasable_text', __( 'Read More', 'woocommerce' ) );
           }
           break;
           }

           // If there is a simple product.
           if ( $product->get_type() == 'simple' ) {
           ?>
           <form action="<?php echo esc_url( $product->add_to_cart_url() ); ?>" class="cart" method="post" enctype="multipart/form-data">
           <?php
           // Displays the quantity box.
           woocommerce_quantity_input();

           // Display the submit button.
           echo sprintf( '<button type="submit" data-product_id="%s" data-product_sku="%s" data-quantity="1" class="%s button product_type_simple">%s</button>', esc_attr( $product->get_id() ), esc_attr( $product->get_sku() ), esc_attr( $link['class'] ), esc_html( $link['label'] ) );
           ?>
           </form>
           <?php
           } else {
           echo apply_filters( 'woocommerce_loop_add_to_cart_link', sprintf('<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="%s button product_type_%s">%s</a>', esc_url( $link['url'] ), esc_attr( $product->get_id() ), esc_attr( $product->get_sku() ), esc_attr( $link['class'] ), esc_attr( $product->get_type() ), esc_html( $link['label'] ) ), $product, $link );
           }

           ?>

          <?php endif; ?>
        </div> <!-- .add-to-cart-button -->
      </div>
    </div>
  </div>
</div> <!-- .section.purchase -->

<div class="section product-spec">

  <div class="desktop show-for-large">

    <div class="product-spec-buttons">
      <div class="grid-x">
        <div class="large-auto cell spec-1 accordion-trigger" data-target="product-spec-content-1">
          <div class="overlay"></div>
          <p class="text-center content">
            <span class="button font-white font-serif">Natural</span> <br>
            <span class="text font-bold font-serif">Natural wood-log cultivation<br>100% made in Japan</span>
          </p>
        </div>
        <div class="large-4 cell spec-2 accordion-trigger" data-target="product-spec-content-2">
          <div class="overlay"></div>
          <p class="text-center content">
            <span class="button font-white font-serif">Pure</span> <br>
            <span class="text font-bold font-serif">Passed 30 pesticide residual items test</span>
          </p>
        </div>
        <div class="large-auto cell spec-3 accordion-trigger" data-target="product-spec-content-3">
          <div class="overlay"></div>
          <p class="text-center content">
            <span class="button font-white font-serif">Essence</span> <br>
            <span class="text font-bold font-serif">16.6:1 extract</span>
          </p>
        </div>
      </div>
    </div>
    <div class="product-spec-expandable">
      <div class="product-spec-content product-spec-content-1">
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <h5 class="text-center font-serif font-white font-bold">Pioneered in natural wood-log cultivation by Japan's foremost reishi growers</h5>
            <p class="text-center font-white">Wild grown reishi is vulnerable to environment pollution and insect infection, which consequently diminish reishi's medical value.<br>Mikei Red Reishi is and end product produced by Japan's foremost reishi grower, the Mayuzumi family who adapts strict and unique cultivation method to produce superior quality reishi.</p>
          </div>
        </div>
        <div class="bg-red-darker">
          <div class="col-full">
            <div class="grid-x grid-margin-x align-middle">
              <div class="small-12 medium-shrink cell">
                <div class="text-center">
                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/4_mayuzumi.jpg" alt="">
                </div>
              </div>
              <div class="small-12 medium-auto cell">
                <h5 class="text-center font-serif font-white">Mayuzumi, the chairman of Japan Reishi Association, continues to uphold his long family traditional of growing high quality mushrooms.</h5>
              </div>
            </div>
          </div>
        </div>
        <div class="bg-red-lighter padding-top-bottom-2em font-white">
          <div class="col-full">
            <div class="grid-x align-middle">
              <div class="small-12 large-6 cell text-left">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/reishicomparisontableeng.png" alt="">
                <p class="text-left" style="margin-top: 20px; max-width: 500px;">Wood log cultivation / Wild Grown</p>
              </div>
              <div class="small-12 large-6 cell text-center">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/wood-log-1.png" alt="">
                <p class="text-left" style="margin: 0 auto; margin-top: 20px; max-width: 250px;">Wood log cultivation allows reishi to fully grow naturally, producing bigger-size fruiting bodies with higher potency.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="product-spec-content product-spec-content-2">
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <h5 class="text-center font-serif font-white font-bold">Double quality assurance from Hong Kong and Japan</h5>
            <div class="text-center">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/No-pesticide-pure-essence.png" alt="">
            </div>
          </div>
        </div>
        <div class="bg-red-darker">
          <div class="col-full padding-top-bottom-2em">
            <p class="text-center font-white"><strong>Mikei&reg; Red Reishi</strong> undergoes a series of rigorous safety tests from cultivation, production, exports to imports, ensuring the quality is safe from human or environmental contaminations. All raw materials required for cultivation including water, soil and wood logs have passed the safety testing of pesticides residues, heavy metals, radioactivity and microbiology etc (total of 340 test items*) conducted by Shokukanken Inc. Environmental and Hygiene Laboratory, Japan. Before the export of the finished products, every batch is required to undergo additional safety test by accredited testing laboratories, JFRL and NKKK, in Japan. At the import into Hong Kong, both the Customs and Excise Department and the Chinese Medicine Council enhance testing of the products to ensure quality is satisfactory.</p>
          </div>
        </div>
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <p class="text-center font-white">Without the use of agricultural chemicals or pesticides, natural wood-log cultivation is the first and foremost technique of using aged oak wood-logs to grow fully mature and superior quality reishi. All raw materials have passed the test of 340 pesticides by Japan Food Research Laboratory.</p>
            <p class="text-center"><a href="https://www.nk1000.com/userfiles/file/Pesticide residues test (Lot 1300).pdf" onclick="window.open(this.href,'','resizable=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,fullscreen=no,dependent=no,status'); return false" class="font-white">August 2016</a></p>
            <p class="text-center"><a href="https://www.nk1000.com/userfiles/file/pesticide%20residues%20test%20(lot%201290).pdf" onclick="window.open(this.href,'','resizable=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,fullscreen=no,dependent=no,status'); return false" class="font-white">July 2016</a></p>
            <div class="text-center">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Mikeitest_engforweb.png" alt="">
            </div>
            <p class="font-white text-center">*Our product safety testing in Japan covers 85 more test items of pesticide residuals than that required by the &ldquo;Pesticide Residuals in Food Regulation&rdquo; of the Center for Food Safety of Hong Kong.</p>
          </div>
        </div>
        <div class="bg-red-darker">
          <div class="col-full padding-top-bottom-2em font-white text-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/test-tube-comparison-en.png" alt=""><br>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Mikei-essence-benefit.jpg" alt="">
          </div>
        </div>
        <div class="bg-red-lighter">
          <div class="grid-x align-center align-middle">
            <div class="shrink cell">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Mikei-finest-powder.png" alt="">
            </div>
            <div class="shrink cell">
              <ul class="font-white">
                <li><span style="">100％溶於水，易吸收</span></li>
                <li><span style="">日本&ldquo;熱水抽出法&rdquo;提煉精華粉末</span></li>
                <li><span style="">靈芝藥味濃郁</span></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="bg-red-darker padding-top-bottom-2em">
          <h5 class="text-center font-serif font-white font-bold">Quality Assurance</h5>
          <p class="font-white text-center">Mikei&reg; Red Reishi </span></strong><span style="font-size: 14px;">is certified by by Japan Reishi Association, an organization supported by industry leaders and government organizations dedicated to maintain the highest standards of product quality and &nbsp;business practices in the Reishi industry worldwide by informing consumers about Japan Reishi health food products and manufacturers.</p>
          <div class="text-center margin-top-1em">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Japan_Reishi_Asso_logo_black_200x114.jpg" alt="">
          </div>
        </div>
      </div>
      <div class="product-spec-content product-spec-content-3">
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <h5 class="text-center font-serif font-white font-bold">Hot water extracted reishi essence</h5>
            <p class="font-white text-center">Using the advance hot water extraction method in Japan, <strong>Mikei<sup>&reg;</sup> Red Reishi</strong> produces the most concentrated reishi essence of 16.6:1 ratio extract that is highly purified and sterilized, which provides the most active compounds, polysaccharides, among other reishi products on the market.</p>
            <p class="font-white text-center">*In 2000, the consumer council of HK tested over 32 reishi products on market in various dose forms including capsule, tablet, powder and granule. <a href="https://www.nk1000.com/userfiles/file/test report.jpg" class="font-white">The test report</a> has confirmed that Mikei<sup>&reg;</sup> Red Reishi contains the highest amount of polysaccharides amongst all.</p>
            <div class="text-center">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/reishiessence5.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>

  </div> <!-- .desktop -->


  <div class="mobile hide-for-large">

    <div class="product-spec-buttons">
      <div class="grid-x">
        <div class="large-auto cell spec-1 accordion-trigger" data-target="product-spec-content-1" data-multiOpen='true'>
          <div class="overlay"></div>
          <p class="text-center content">
            <span class="button font-white font-serif">Natural</span> <br>
            <span class="text font-bold font-serif">Natural wood-log cultivation<br>100% made in Japan</span>
          </p>
        </div>
      </div>
    </div>
    <div class="product-spec-expandable">
      <div class="product-spec-content product-spec-content-1">
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <h5 class="text-center font-serif font-white font-bold">Pioneered in natural wood-log cultivation by Japan's foremost reishi growers</h5>
            <p class="text-center font-white">Wild grown reishi is vulnerable to environment pollution and insect infection, which consequently diminish reishi's medical value.</p>
            <p class="font-white">Mikei Red Reishi is and end product produced by Japan's foremost reishi grower, the Mayuzumi family who adapts strict and unique cultivation method to produce superior quality reishi.</p>
          </div>
        </div>
        <div class="bg-red-darker">
          <div class="col-full">
            <div class="grid-x grid-margin-x align-middle">
              <div class="small-12 medium-shrink cell">
                <div class="text-center">
                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/4_mayuzumi.jpg" alt="">
                </div>
              </div>
              <div class="small-12 medium-auto cell">
                <h5 class="text-center font-serif font-white">Mayuzumi, the chairman of Japan Reishi Association, continues to uphold his long family traditional of growing high quality mushrooms.</h5>
              </div>
            </div>
          </div>
        </div>
        <div class="bg-red-lighter padding-top-bottom-2em font-white">
          <div class="col-full">
            <div class="grid-x align-middle">
              <div class="small-12 large-6 cell text-left">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/reishicomparisontableeng.png" alt="">
                <p class="text-left" style="margin-top: 20px; max-width: 500px;">Wood log cultivation / Wild Grown</p>
              </div>
              <div class="small-12 large-6 cell text-center">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/wood-log-1.png" alt="">
                <p class="text-left" style="margin: 0 auto; margin-top: 20px; max-width: 250px;">Wood log cultivation allows reishi to fully grow naturally, producing bigger-size fruiting bodies with higher potency.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="product-spec-buttons">
      <div class="grid-x">
        <div class="large-4 cell spec-2 accordion-trigger" data-target="product-spec-content-2"  data-multiOpen='true'>
          <div class="overlay"></div>
          <p class="text-center content">
            <span class="button font-white font-serif">Pure</span> <br>
            <span class="text font-bold font-serif">Passed 30 pesticide residual items test</span>
          </p>
        </div>
      </div>
    </div>
    <div class="product-spec-expandable">
      <div class="product-spec-content product-spec-content-2">
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <h5 class="text-center font-serif font-white font-bold">Double quality assurance from Hong Kong and Japan</h5>
            <div class="text-center">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/No-pesticide-pure-essence.png" alt="">
            </div>
          </div>
        </div>
        <div class="bg-red-darker">
          <div class="col-full padding-top-bottom-2em">
            <p class="text-center font-white" style="margin: 0 auto; max-width: 550px;">Mikei® Red Reishi undergoes a series of rigorous safety tests from cultivation, production, exports to imports, ensuring the quality is safe from human or environmental contaminations. All raw materials required for cultivation including water, soil and wood logs have passed the safety testing of pesticides residues, heavy metals, radioactivity and microbiology etc (total of 340 test items*) conducted by Shokukanken Inc. Environmental and Hygiene Laboratory, Japan. Before the export of the finished products, every batch is required to undergo additional safety test by accredited testing laboratories, JFRL and NKKK, in Japan. At the import into Hong Kong, both the Customs and Excise Department and the Chinese Medicine Council enhance testing of the products to ensure quality is satisfactory.</p>
          </div>
        </div>
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <p class="text-center font-white">Without the use of agricultural chemicals or pesticides, natural wood-log cultivation is the first and foremost technique of using aged oak wood-logs to grow fully mature and superior quality reishi. All raw materials have passed the test of 340 pesticides by Japan Food Research Laboratory.</p>
            <p class="text-center"><a href="https://www.nk1000.com/userfiles/file/Pesticide residues test (Lot 1300).pdf" onclick="window.open(this.href,'','resizable=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,fullscreen=no,dependent=no,status'); return false" class="font-white">August 2016</a></p>
            <p class="text-center"><a href="https://www.nk1000.com/userfiles/file/pesticide%20residues%20test%20(lot%201290).pdf" onclick="window.open(this.href,'','resizable=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,fullscreen=no,dependent=no,status'); return false" class="font-white">July 2016</a></p>
            <div class="text-center">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Mikeitest_engforweb.png" alt="">
            </div>
            <p class="font-white text-center">*Our product safety testing in Japan covers 85 more test items of pesticide residuals than that required by the “Pesticide Residuals in Food Regulation” of the Center for Food Safety of Hong Kong.</p>
          </div>
        </div>
        <div class="bg-red-darker">
          <div class="col-full padding-top-bottom-2em font-white text-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/test-tube-comparison-en.png" alt=""><br>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Mikei-essence-benefit.jpg" alt="">
          </div>
        </div>
        <div class="bg-red-lighter">
          <div class="grid-x align-center align-middle">
            <div class="shrink cell">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Mikei-finest-powder.png" alt="">
            </div>
            <div class="shrink cell">
              <ul class="font-white">
                <li><span style="">100％溶於水，易吸收</span></li>
                <li><span style="">日本&ldquo;熱水抽出法&rdquo;提煉精華粉末</span></li>
                <li><span style="">靈芝藥味濃郁</span></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="bg-red-darker padding-top-bottom-2em">
          <h5 class="text-center font-serif font-white font-bold">Quality Assurance</h5>
          <p class="font-white text-center">Mikei® Red Reishi is certified by by Japan Reishi Association, an organization supported by industry leaders and government organizations dedicated to maintain the highest standards of product quality and  business practices in the Reishi industry worldwide by informing consumers about Japan Reishi health food products and manufacturers.</p>
          <div class="text-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Japan_Reishi_Asso_logo_black_200x114.jpg" alt="">
          </div>
        </div>
      </div>
    </div>

    <div class="product-spec-buttons">
      <div class="grid-x">
        <div class="large-auto cell spec-3 accordion-trigger" data-target="product-spec-content-3"  data-multiOpen='true'>
          <div class="overlay"></div>
          <p class="text-center content">
            <span class="button font-white font-serif">Essence</span> <br>
            <span class="text font-bold font-serif">16.6:1 extract</span>
          </p>
        </div>
      </div>
    </div>
    <div class="product-spec-expandable">
      <div class="product-spec-content product-spec-content-3">
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <h5 class="text-center font-serif font-white font-bold">Hot water extracted reishi essence</h5>
            <p class="font-white text-center">Using the advance hot water extraction method in Japan, Mikei<sup>&reg;</sup> Red Reishi produces the most concentrated reishi essence of 16.6:1 ratio extract that is highly purified and sterilized, which provides the most active compounds, polysaccharides, among other reishi products on the market.</p>
            <p class="font-white text-center">*In 2000, the consumer council of HK tested over 32 reishi products on market in various dose forms including capsule, tablet, powder and granule. <a class="font-white" href="https://www.nk1000.com/userfiles/file/test report.jpg">The test report</a> has confirmed that <strong>Mikei<sup>&reg;</sup> Red Reishi</strong> contains the highest amount of polysaccharides amongst all.</p>
            <div class="text-center">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/reishiessence5.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>

  </div> <!-- .mobile -->

</div> <!-- .section.product-spec -->

<div class="section faq">
  <div class="bg-yellow-pale padding-top-4em padding-bottom-4em">
    <div class="col-full">
      <h3 class="text-center font-black font-serif font-wider">How to Distinguish Superior Red Reishi?</h3>
      <div class="grid-x grid-margin-x margin-top-2em padding-bottom-5em">
        <div class="small-6 large-3 cell text-center">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/npk-redresi-pdtpage-v6-03.png" alt="">
          <h5 class="font-serif font-gold font-bold margin-top-1em">Scent<br>A Rich and pure favor</h5>
        </div>
        <div class="small-6 large-3 cell text-center">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/npk-redresi-pdtpage-v6-04.png" alt="">
          <h5 class="font-serif font-gold font-bold margin-top-1em">Colour<br>Clear and pure liquid</h5>
        </div>
        <div class="small-6 large-3 cell text-center">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/npk-redresi-pdtpage-v6-05.png" alt="">
          <h5 class="font-serif font-gold font-bold margin-top-1em">Taste<br>Melt in one's month, bitter to bittersweet taste</h5>
        </div>
        <div class="small-6 large-3 cell text-center">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/npk-redresi-pdtpage-v6-06.png" alt="">
          <h5 class="font-serif font-gold font-bold margin-top-1em">Touch<br>Extremely fine texture</h5>
        </div>
      </div>
      <h3 class="text-center font-black font-serif font-wider">Marched into London's Famous Department Store "Harrods" with 20 Years of Notable Reputation</h3>
      <p class="text-center font-gold" style="max-width: 600px; margin: 0 auto;">Since the first day of sales, Mikei<sup>&reg;</sup> Red Reishi has been selling in 15 worldwide countries with retail outlets spread through Asia, North America and Europe. Recently it has successfully marched into London's famous department store, Harrods, which carries most of the historic brands in London.</p>
      <div class="text-center">
        <img class="margin-top-2em padding-bottom-4em" src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/har2.jpg" alt="">
      </div>
      <h3 class="text-center font-black font-serif font-wider">FAQ</h3>
      <ul class="accordion" data-accordion data-allow-all-closed="true">
        <li class="accordion-item" data-accordion-item>
          <a href="#" class="accordion-title">
            <h3 class="text-left font-serif font-gold margin-0">Q: What is Mikei® Red Reishi?</h3>
          </a>
          <div class="accordion-content" data-tab-content>
            <p>A: Although Mikei® Red Reishi is considered as a supreme supplement in China and Japan. Since a long time ago, royal families in both China and Japan consumed red reishi as a supplement for their healthy well-being. Mikei® Red Reishi has the ability of boosting your immune system and regulating your body’s function; it also carries the effects of anti-aging. Effects are easily noticeable when Mikei® Red Reishi is comsumed regularly.</p>
          </div>
        </li>
        <li class="accordion-item" data-accordion-item>
          <a href="#" class="accordion-title">
            <h3 class="text-left font-serif font-gold margin-0">Q: Can I take Mikei® Red Reishi with other medications?</h3>
          </a>
          <div class="accordion-content" data-tab-content>
            <p>A: Although Mikei® Red Reishi is a natural health supplement, it is recommended that you consult a healthcare practitioner prior to taking any new medication. There have been no reports to show that red reishi cannot be taken with any other medications. However, patients undergoing organ transplants and using immuno-suppressive drugs should always be cautious when taking any immune-modulating substances such as red reishi and are advised to consult their healthcare practitioner.</p>
          </div>
        </li>
        <li class="accordion-item" data-accordion-item>
          <a href="#" class="accordion-title">
            <h3 class="text-left font-serif font-gold margin-0">Q: Is red reishi safe for everyone to take?</h3>
          </a>
          <div class="accordion-content" data-tab-content>
            <p>A: There are no studies that restrict the consumption of red reishi to any specific person. However, consulting a health care practitioner prior to taking any health supplement is recommended especially for those who are pregnant or breastfeeding.</p>
          </div>
        </li>
        <li class="accordion-item" data-accordion-item>
          <a href="#" class="accordion-title">
            <h3 class="text-left font-serif font-gold margin-0">Q: Are there any side-effects?</h3>
          </a>
          <div class="accordion-content" data-tab-content>
            <p>A: Red reishi is a natural substance that can be taken for a long period of time without any side effects. There have been cases where sensitive individuals may experience detoxification symptoms such as mild upset stomach, dryness of the nose and mouth, dizziness, soreness, and skin rashes. These symptoms normally subside after a couple of days. Consult a health care practitioner if these symptoms persist.</p>
          </div>
        </li>
        <li class="accordion-item" data-accordion-item>
          <a href="#" class="accordion-title">
            <h3 class="text-left font-serif font-gold margin-0">Q: How soon can one start noticing results?</h3>
          </a>
          <div class="accordion-content" data-tab-content>
            <p>A: Results can vary from a week to several months with the daily intake of a high quality reishi product such as Mikei, but most people notice a difference to their overall health and well-being within a month.</p>
          </div>
        </li>
        <li class="accordion-item" data-accordion-item>
          <a href="#" class="accordion-title">
            <h3 class="text-left font-serif font-gold margin-0">Q: How long can one take Mikei® Red Reishi for?</h3>
          </a>
          <div class="accordion-content" data-tab-content>
            <p>A: There is no limit as to how long one can take Mikei® Red Reishi. The daily consumption of red reishi is as much of a preventative measure for good health as it is a tool for people with specific health concerns.</p>
          </div>
        </li>
        <li class="accordion-item" data-accordion-item>
          <a href="#" class="accordion-title">
            <h3 class="text-left font-serif font-gold margin-0">Q: Are Mikei® Red Reishi capsules vegetarian friendly?</h3>
          </a>
          <div class="accordion-content" data-tab-content>
            <p>A: Yes! Mikei® Red Reishi capsules are all-natural and suitable for a variety of dietary requirements including vegetarians, diabetics, and those on restricted diets.</p>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<script>
var acc = document.getElementsByClassName("accordion-trigger");
var i;
var panels = document.getElementsByClassName("product-spec-content");

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {

    var targetPanels = document.getElementsByClassName(this.dataset.target);
    
    // Toggle trigger active state
    this.classList.toggle("active");
    
    if ( this.classList.contains("active") ) {
      if (!this.dataset.multiopen) {
        // Close all panels first
        for (j = 0; j < panels.length; j++) {
          panels[j].style.maxHeight = null;
        }
        // Clear Trigger Active state
        for (j = 0; j < acc.length; j++) {
          acc[j].classList.remove("active");
        }
        // Add back the active state
        this.classList.add("active");
      }
      // Open the target panels
      for (k = 0; k < targetPanels.length; k++) {
        targetPanels[k].style.maxHeight = targetPanels[k].scrollHeight + "px";
      }
    } else {
      // Close the target panels
      for (k = 0; k < targetPanels.length; k++) {
        targetPanels[k].style.maxHeight = null;
      }
    }

  });
}
</script>