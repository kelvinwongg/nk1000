<?php
// 
// functions/storefront-hooks.php
// 
function storefront_child_remove_storefront_hooks () {
  remove_action( 'homepage', 'storefront_homepage_content', 10 );
  remove_action( 'homepage', 'storefront_product_categories', 20 );
  remove_action( 'homepage', 'storefront_recent_products', 30 );
  // remove_action( 'homepage', 'storefront_featured_products', 40 );
  remove_action( 'homepage', 'storefront_popular_products', 50 );
  remove_action( 'homepage', 'storefront_on_sale_products', 60 );
  remove_action( 'homepage', 'storefront_best_selling_products', 70 );
  remove_action( 'storefront_footer', 'storefront_credit', 20 );
  // remove_action( 'storefront_footer', 'storefront_handheld_footer_bar', 999 );
  remove_action( 'storefront_single_post', 'storefront_post_meta', 20 );
  remove_action( 'storefront_single_post_bottom', 'storefront_display_comments', 20 );
  remove_action( 'storefront_before_content','woocommerce_breadcrumb', 10 );
}
add_action('init', 'storefront_child_remove_storefront_hooks');

if ( ! function_exists( 'storefront_secondary_navigation' ) ) {
  /**
   * Display Secondary Navigation
   *
   * @since  1.0.0
   * @return void
   */
  function storefront_secondary_navigation() {
    ?>
    <nav class="secondary-navigation" role="navigation" aria-label="<?php esc_html_e( 'Secondary Navigation', 'storefront' ); ?>">
    
    <ul class="menu">
      <li class="menu-item">
        <select id="lang-switch">
          <?php
          $wpml_languages = apply_filters( 'wpml_active_languages', NULL);
          foreach ($wpml_languages as $this_lang_code => $this_lang) { ?>
            <option
              value="<?php echo $this_lang['url']; ?>"
              <?php if (ICL_LANGUAGE_CODE == $this_lang_code) echo 'selected="selected"'; ?>
              ><?php echo $this_lang['native_name']; ?></option>
          <?php } ?>
        </select>
      </li>
      <li class="menu-item">
        <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">
        <?php
          if ( is_user_logged_in() ) {
            global $current_user; wp_get_current_user();
            echo $current_user->display_name . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . __('My Account');
          } else {
            _e("Login / Register");
          }
        ?>
        </a>
      </li>
    </ul>
    <?php
    if ( has_nav_menu( 'secondary' ) ) {
      wp_nav_menu(
        array(
          'theme_location'  => 'secondary',
          'fallback_cb'   => '',
        )
      );
    }
    ?></nav><!-- #site-navigation --><?php
  }
}

if (!function_exists('storefront_child_product_hero_slider')) {
  function storefront_child_product_hero_slider() {
    echo get_template_part_arg('partials/storefront_child_product_hero_slider', array(
      'hero_slider' => get_field("hero_slider"),
    ));
  }
  add_action( 'homepage', 'storefront_child_product_hero_slider', 15 );
}

if ( ! function_exists( 'storefront_featured_products' ) ) {
  /**
   * Display Featured Products
   * Hooked into the `homepage` action in the homepage template
   *
   * @since  1.0.0
   * @param array $args the product section args.
   * @return void
   */
  function storefront_featured_products( $args ) {

    global $disallowed_products;
    // xd($disallowed_products);

    if ( storefront_is_woocommerce_activated() ) {

      echo '<section class="storefront-product-section storefront-featured-products" aria-label="' . esc_attr__( 'Featured Products', 'storefront' ) . '">';

      do_action( 'storefront_homepage_before_featured_products' );

      echo '<h2 class="section-title">' . wp_kses_post( __( 'We Recommend', 'storefront' ) ) . '</h2>';

      do_action( 'storefront_homepage_after_featured_products_title' );

      ?><ul class="products"><?php
        $args = array(
          'post_type' => 'product',
          'posts_per_page' => 12,
          'tax_query' => array(
              array(
                'taxonomy' => 'product_visibility',
                'field'    => 'name',
                'terms'    => 'featured',
              ),
            ),
          );
        $loop = new WP_Query( $args );
        if ( $loop->have_posts() ) {
          while ( $loop->have_posts() ) : $loop->the_post();
            wc_get_template_part( 'content', 'product' );
          endwhile;
        } else {
          echo __( 'No products found' );
        }
        wp_reset_postdata();
      ?></ul><!--/.products--><?php

      do_action( 'storefront_homepage_after_featured_products' );

      echo '</section>';

    }
  }
}

if (!function_exists('storefront_child_page_banner')) {
  function storefront_child_page_banner() {
    global $post;
    if (is_page() && !is_front_page()) {
      echo get_template_part_arg('partials/storefront_child_page_banner', array(
        'post_id' => $post->ID,
      ));
    }
  }
  add_action( 'storefront_content_top', 'storefront_child_page_banner', 10 );
}

if ( ! function_exists( 'storefront_child_credit' ) ) {
  function storefront_child_credit() {
    echo get_template_part_arg('partials/storefront_child_credit', array());
  }
  add_action( 'storefront_footer', 'storefront_child_credit', 19 );
}

if ( ! function_exists( 'storefront_footer_widgets' ) ) {
  /**
   * Display the footer widget regions.
   *
   * @since  1.0.0
   * @return void
   */
  function storefront_footer_widgets() {
    $rows    = intval( apply_filters( 'storefront_footer_widget_rows', 1 ) );
    $regions = intval( apply_filters( 'storefront_footer_widget_columns', 4 ) );

    for ( $row = 1; $row <= $rows; $row++ ) :

      ?><div class="col-full"><?php
      // Defines the number of active columns in this footer row.
      for ( $region = $regions; 0 < $region; $region-- ) {
        if ( is_active_sidebar( 'footer-' . strval( $region + $regions * ( $row - 1 ) ) ) ) {
          $columns = $region;
          break;
        }
      }

      if ( isset( $columns ) ) : ?>
        <div class=<?php echo '"footer-widgets row-' . strval( $row ) . ' col-' . strval( $columns ) . ' fix"'; ?>><?php

          for ( $column = 1; $column <= $columns; $column++ ) :
            $footer_n = $column + $regions * ( $row - 1 );

            if ( is_active_sidebar( 'footer-' . strval( $footer_n ) ) ) : ?>

              <div class="block footer-widget-<?php echo strval( $column ); ?>">
                <?php dynamic_sidebar( 'footer-' . strval( $footer_n ) ); ?>
              </div><?php

            endif;
          endfor; ?>

        </div><!-- .footer-widgets.row-<?php echo strval( $row ); ?> --><?php

        unset( $columns );
      endif;
      ?></div><?php
    endfor;
  }
}

if ( ! function_exists( 'storefront_page_header' ) ) {
  /**
   * Display the page header
   *
   * @since 1.0.0
   */
  function storefront_page_header() {
    ?>
    <header class="entry-header">
      <?php
      // storefront_post_thumbnail( 'full' );
      the_title( '<h1 class="entry-title">', '</h1>' );
      ?>
    </header><!-- .entry-header -->
    <?php
  }
}

// Single Product disable sorting
if ( ! function_exists( 'storefront_sorting_wrapper' ) ) {
  /**
   * Sorting wrapper
   *
   * @since   1.4.3
   * @return  void
   */
  function storefront_sorting_wrapper() {
    echo '<div class="storefront-sorting" style="display:none;">';
  }
}

/**
 * Rename product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

  $tabs['description']['title'] = __( 'Benefits' );   // Rename the description tab
  // $tabs['reviews']['title'] = __( 'Ratings' );        // Rename the reviews tab
  // $tabs['additional_information']['title'] = __( 'Product Data' );  // Rename the additional information tab

  return $tabs;

}

/**
 * Customize product data tabs
 */
// add_filter( 'woocommerce_product_tabs', 'woo_custom_description_tab', 98 );
function woo_custom_description_tab( $tabs ) {

  $tabs['description']['callback'] = 'woo_custom_description_tab_content';  // Custom description callback

  return $tabs;
}

function woo_custom_description_tab_content() {
  echo '<h2>Custom Description</h2>';
  echo '<p>Here\'s a custom description</p>';
}

/**
 * Add a custom product data tab
 */
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {
  
  // Adds the new tab
  
  if (get_field("quality")) {
    $tabs['quality'] = array(
      'title'   => __( 'Quality', 'woocommerce' ),
      'priority'  => 11,
      'callback'  => 'woo_quality_product_tab_content'
    );
  }

  if (get_field("specifications")) {
    $tabs['specifications'] = array(
      'title'   => __( 'Specifications', 'woocommerce' ),
      'priority'  => 12,
      'callback'  => 'woo_specifications_product_tab_content'
    );
  }

  if (get_field("ichiban_quality")) {
    $tabs['ichiban_quality'] = array(
      'title'   => __( 'Ichiban Quality', 'woocommerce' ),
      'priority'  => 13,
      'callback'  => 'woo_ichiban_quality_product_tab_content'
    );
  }

  if (get_field("nissan_chem_co")) {
    $tabs['nissan_chem_co'] = array(
      'title'   => __( 'Nissan Chem Co.', 'woocommerce' ),
      'priority'  => 13,
      'callback'  => 'woo_nissan_chem_co_product_tab_content'
    );
  }

  if (get_field("faq")) {
    $tabs['faq'] = array(
      'title'   => __( 'FAQ', 'woocommerce' ),
      'priority'  => 13,
      'callback'  => 'woo_faq_product_tab_content'
    );
  }

  return $tabs;

}

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {
  // Remove the additional information tab
  unset( $tabs['additional_information'] );
  return $tabs;
}

function woo_quality_product_tab_content() {
  echo get_field("quality");
}
function woo_specifications_product_tab_content() {
  echo get_field("specifications");
}
function woo_ichiban_quality_product_tab_content() {
  echo get_field("ichiban_quality");
}
function woo_nissan_chem_co_product_tab_content() {
  echo get_field("nissan_chem_co");
}
function woo_faq_product_tab_content() {
  echo get_field("faq");
}

// START: Hook into Catalog Visibility Option
function my_alternate_button($content) {
  return '<div class="text_for_product_na_in_country">' . get_field("text_for_product_na_in_country") . '</div>';
}
add_filter('catalog_visibility_alternate_add_to_cart_button', 'my_alternate_button', 10, 1);

function my_alternate_price_text($content) {
  return (get_field("price_replacement_for_product_na_in_country")) ? get_field("price_replacement_for_product_na_in_country") : "&nbsp;";;
}
add_filter('catalog_visibility_alternate_price_html', 'my_alternate_price_text', 10, 1);

function exclude_cvo_disallowed_menu_items( $items, $menu, $args ) {
  if (!is_admin()) {
    // Query Disallowed Products (By Location?)
    global $disallowed_products;
    // Iterate over the items to search and remove
    foreach ( $items as $key => $item ) {
      if ( in_array($item->object_id, $disallowed_products) ) unset( $items[$key] );
    }
  }
  return $items;
}
add_filter( 'wp_get_nav_menu_items', 'exclude_cvo_disallowed_menu_items', null, 3 );

// Global varialbe for disallowed products
$disallowed_products;

function global_disallowed_products () {
  if (!is_admin()) {
    global $disallowed_products;
    $wc_catalog_restrictions_query = new WC_Catalog_Restrictions_Query;
    $disallowed_products = $wc_catalog_restrictions_query->get_disallowed_products();
    // foreach ($disallowed_products as $key => $this_product_id) {
    //   $disallowed_products[$key] = apply_filters( 'wpml_object_id', $this_product_id, 'post', TRUE  );
    // }
  }
  // echo 'DEBUG INFORMATION: WC_Catalog_Restrictions_Query->get_disallowed_products';
  // xd($disallowed_products);
}
add_action('init', 'global_disallowed_products');

function always_exclude_disallowed_products( $query ) {
  // if ( $query->is_main_query() ) {
    global $disallowed_products;
    $query->set( 'post__not_in',$disallowed_products );
  // }
}
add_action( 'pre_get_posts', 'always_exclude_disallowed_products' );
// END: Hook into Catalog Visibility Option

// START: Replace WC Breadcrumb with Yoast's
function nk1000_yoast_breadcrumb() {
  if ( function_exists('yoast_breadcrumb') && !is_front_page() ) {
    yoast_breadcrumb('<div class="storefront-breadcrumb"><div class="col-full"><nav class="woocommerce-breadcrumb">','</nav></div></div>');
  }
}
add_action('storefront_before_content', 'nk1000_yoast_breadcrumb');
// END: Replace WC Breadcrumb with Yoast's

// START: Add NK1000 GTM
function add_nk1000_gtm() {
?><!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5NLJFT6');</script>
<!-- End Google Tag Manager --><?php
}
add_action('wp_head', 'add_nk1000_gtm');

function add_nk1000_gtm_2() {
?><!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5NLJFT6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) --><?php
}
add_action('storefront_before_site', 'add_nk1000_gtm_2');
// END: Add NK1000 GTM

// START: Single Product Related Product
if ( ! function_exists( 'woocommerce_related_products' ) ) {

  /**
   * Output the related products.
   *
   * @param array $args Provided arguments.
   */
  function woocommerce_related_products( $args = array() ) {
    global $product;
    global $disallowed_products;

    if ( ! $product ) {
      return;
    }

    $defaults = array(
      'posts_per_page' => 2,
      'columns'        => 2,
      'orderby'        => 'rand', // @codingStandardsIgnoreLine.
      'order'          => 'desc',
    );

    $args = wp_parse_args( $args, $defaults );

    $exclude_ids = $product->get_upsell_ids();
    $exclude_ids = array_merge($exclude_ids, $disallowed_products);

    // Get visible related products then sort them at random.
    $args['related_products'] = array_filter( array_map( 'wc_get_product', wc_get_related_products( $product->get_id(), $args['posts_per_page'], $exclude_ids ) ), 'wc_products_array_filter_visible' );

    // Handle orderby.
    $args['related_products'] = wc_products_array_orderby( $args['related_products'], $args['orderby'], $args['order'] );

    // Set global loop values.
    wc_set_loop_prop( 'name', 'related' );
    wc_set_loop_prop( 'columns', apply_filters( 'woocommerce_related_products_columns', $args['columns'] ) );

    wc_get_template( 'single-product/related.php', $args );
  }
}
// END: Single Product Related Product

// START: Script for Mikei product template
function insertcart_wc_loop_add_to_cart_scripts() {
 if ( is_shop() || is_product_category() || is_product_tag() || is_product() ) : ?>

<script>
 jQuery( document ).ready( function( $ ) {
 $( document ).on( 'change', '.quantity .qty', function() {
 $( this ).parent( '.quantity' ).next( '.add_to_cart_button' ).attr( 'data-quantity', $( this ).val() );
 });
 });
</script>

 <?php endif;
}

add_action( 'wp_footer', 'insertcart_wc_loop_add_to_cart_scripts', 999 );
// END: Script for Mikei product template

// START: WC Password Strength
/**
 * Change the strength requirement on the woocommerce password
 *
 * Strength Settings
 * 4 = Strong
 * 3 = Medium (default) 
 * 2 = Also Weak but a little stronger 
 * 1 = Password should be at least Weak
 * 0 = Very Weak / Anything
 */
add_filter( 'woocommerce_min_password_strength', 'nk1000_change_password_strength' );
 
function nk1000_change_password_strength( $strength ) {
   return 1;
}
// END: WC Password Strength

// START: Modify Cart Template
function nk100_add_gift_redeem_to_cart_page() {
  wc_get_template_part( 'content', 'gift-redeem' );
}
add_action('woocommerce_cart_collaterals', 'nk100_add_gift_redeem_to_cart_page', 9);
add_action('woocommerce_after_order_notes', 'nk100_add_gift_redeem_to_cart_page', 9);
// END: Modify Cart Template

// START: Cart Clear Applied Points Redeem Coupon
function nk1000_remove_redeem_coupon() {
  global $wc_points_rewards;

  if ( isset($_POST['nk1000_points_rewards_apply_discount'])
    && $_POST['nk1000_points_rewards_apply_discount'] == 'Clear Discount') {
    // xd(WC()->cart->get_applied_coupons());
    foreach (WC()->cart->get_applied_coupons() as $this_applied_coupon) {
      // xd(strpos($this_applied_coupon, 'wc_points_redemption'));
      if (strpos($this_applied_coupon, 'wc_points_redemption') === 0) {
        WC()->cart->remove_coupon($this_applied_coupon);
      }
    }
    // Apply Coupon again
    // xd($wc_points_rewards->cart);
    $wc_points_rewards->cart->maybe_apply_discount();
  }
}
add_action('woocommerce_before_cart', 'nk1000_remove_redeem_coupon');
// END: Cart Clear Applied Points Redeem Coupon

// START: Add 'Points Required to Redeem' field to General product tab
// Display Fields
function woo_add_custom_general_fields() {

  global $woocommerce, $post;
  
  echo '<div class="options_group">';
  
  // Custom fields will be created here...
  // Number Field
  woocommerce_wp_text_input( 
    array( 
      'id'                => '_redeem_points', 
      'label'             => __( 'Points Required to Redeem', 'woocommerce' ), 
      'placeholder'       => '', 
      'description'       => __( 'Enter the points required to redeem this product.', 'woocommerce' ),
      'type'              => 'number', 
      'custom_attributes' => array(
          'step'  => 'any',
          'min' => '0'
        ) 
    )
  );
  
  echo '</div>';
  
}
add_action( 'woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields' );

// Save Fields
function woo_add_custom_general_fields_save( $post_id ){
    
  // Number Field
  $woocommerce_number_field = $_POST['_redeem_points'];
  if( !empty( $woocommerce_number_field ) )
    update_post_meta( $post_id, '_redeem_points', esc_attr( $woocommerce_number_field ) );  

}
add_action( 'woocommerce_process_product_meta', 'woo_add_custom_general_fields_save' );
// END: Add 'Points Required to Redeem' field to General product tab

// START: NK1000_Points_Rewards_Manager
/**
 * NK1000_Points_Rewards_Manager to perform points rewards related function of nk1000.com
 */
class NK1000_Points_Rewards_Manager {
  function __construct( $args ) {}

  public static function calculate_user_cart_points() {
    if (!is_user_logged_in()) return false;
    $user_points_used_gift = 0;
    foreach (WC()->cart->get_cart_contents() as $key => $this_cart_product) {
      if (has_term(['gifts','gifts-sc','gifts-tc'], 'product_cat', $this_cart_product['product_id'])) {
        $user_points_used_gift += $this_cart_product['quantity'] * (int)get_post_meta($this_cart_product['product_id'], '_redeem_points')[0];
      }
    }
    $user_points_used_cash = 0;
    foreach (WC()->cart->coupon_discount_totals as $coupon_code => $this_amount) {
      if (strpos($coupon_code, 'wc_points_redemption') === 0) {
        // xd($this_amount);
        $user_points_used_cash += WC_Points_Rewards_Manager::calculate_points_for_discount( $this_amount );
        // xd($user_points_used_cash);
      }
    }
    $user_points_total = WC_Points_Rewards_Manager::get_users_points( get_current_user_id() );
    $user_points_used_total = $user_points_used_cash + $user_points_used_gift;
    $user_points_remain = $user_points_total - $user_points_used_total;

    return array(
      'points_total' => $user_points_total,
      'points_used_gift' => $user_points_used_gift,
      'points_used_cash' => $user_points_used_cash,
      'points_used_total' => $user_points_used_total,
      'points_remain' => $user_points_remain,
    );
  }

  public static function calculate_user_order_points( $order_id ) {
    $order = wc_get_order( $order_id );
    // xd($order);
    $user_points_used_gift = 0;
    $points_earned = 0;
    $user_points_used_gift_detail = [];
    // xd($order->get_items());
    foreach ($order->get_items() as $item_key => $item) {
      if (has_term(['gifts','gifts-sc','gifts-tc'], 'product_cat', $item->get_product_id())) {
        $user_points_used_gift += $item->get_quantity() * (int)get_post_meta($item->get_product_id(), '_redeem_points')[0];
        array_push($user_points_used_gift_detail, array(
          'name' => $item->get_name(),
          'quantity' => $item->get_quantity(),
          'points_required' => (int)get_post_meta($item->get_product_id(), '_redeem_points')[0],
          'subtotal' => $item->get_quantity() * (int)get_post_meta($item->get_product_id(), '_redeem_points')[0]
        ));
      } else {
        // xd($item->get_product()->get_price());
        $points_earned += ( WC_Points_Rewards_Manager::calculate_points( $item->get_product()->get_price() ) * $item->get_quantity() );
        // $points_earned += apply_filters( 'woocommerce_points_earned_for_cart_item', WC_Points_Rewards_Product::get_points_earned_for_product_purchase( $item->get_data()['product_id'] ), $item_key, $item->get_data()['product_id'] ) * $item->get_quantity();
        // $points_earned += WC_Points_Rewards_Product::get_points_earned_for_product_purchase( $item->get_data()['product_id'] );
      }
    }
    // xd($points_earned);

    $order_data = $order->get_data();
    // xd($order_data['customer_id']);
    $order_metadata = $order->get_meta( '_wc_points_logged_redemption' );
    $user_points_used_cash = $order_metadata['points'];

    $user_points_used_total = $user_points_used_cash + $user_points_used_gift;

    $user_points_total = $order->get_meta( '_wc_points_current_user_total' );
    // xd($user_points_total);
    if ($user_points_total === '') {
      $user_points_total = WC_Points_Rewards_Manager::get_users_points( $order_data['customer_id'] );
      $user_points_total += $user_points_used_total;
    }

    $user_points_earned = $order->get_meta( '_wc_points_earned' );
    // xd($user_points_earned);
    // xd($points_earned);
    if ($user_points_earned === '') {
      $user_points_earned = $points_earned;
    }

    $user_points_remain = $user_points_total - $user_points_used_total + $user_points_earned;
    
    return array(
      'points_total' => $user_points_total,
      'points_used_gift' => $user_points_used_gift,
      'points_used_gift_detail' => $user_points_used_gift_detail,
      'points_used_cash' => $user_points_used_cash,
      'points_used_total' => $user_points_used_total,
      'points_earned' => $user_points_earned,
      'points_remain' => $user_points_remain,
    );
  }
}
function test_calculate_user_order_points () {
  // NK1000_Points_Rewards_Manager::calculate_user_order_points( 1145 );
  // xd(NK1000_Points_Rewards_Manager::calculate_user_order_points( 1154 ));
}
add_action('init', 'test_calculate_user_order_points');
function calculate_user_cart_points_ajax () {
  // response output
  header( "Content-Type: application/json" );
  echo json_encode(NK1000_Points_Rewards_Manager::calculate_user_cart_points());
  // IMPORTANT: don't forget to "wp_die"
  wp_die();
}
add_action( 'wp_ajax_calculate_user_cart_points', 'calculate_user_cart_points_ajax' );
// add_action( 'wp_ajax_nopriv_calculate_user_cart_points', 'calculate_user_cart_points_ajax' );
// add_action( 'wc_ajax_calculate_user_cart_points', 'calculate_user_cart_points_ajax' );

// Checking and validating when products are added to cart
function add_to_cart_if_user_has_enough_points( $passed, $product_id, $quantity ) {

  if (!has_term(['gifts','gifts-sc','gifts-tc'], 'product_cat', $product_id)) { return $passed; }

  $user_points_remain = NK1000_Points_Rewards_Manager::calculate_user_cart_points()['points_remain'];
  $points_require_for_this_gift = (int)get_post_meta($product_id, '_redeem_points')[0];

  if ($user_points_remain - $quantity * $points_require_for_this_gift < 0) {
    // Set to false
    $passed = false;
    // Display a message
     wc_add_notice( __( "You do not have enough points to redeem this item.", "woocommerce" ), "error" );
  }
  return $passed;
}
add_filter( 'woocommerce_add_to_cart_validation', 'add_to_cart_if_user_has_enough_points', 10, 3 );

// Checking and validating when updating cart item quantities when products are added to cart
function update_cart_if_user_has_enough_points( $passed, $cart_item_key, $values, $updated_quantity ) {

  if (!has_term(['gifts','gifts-sc','gifts-tc'], 'product_cat', $values['product_id'])) { return $passed; }

  $user_points_remain = NK1000_Points_Rewards_Manager::calculate_user_cart_points()['points_remain'];
  $points_require_for_this_gift = (int)get_post_meta($values['product_id'], '_redeem_points')[0];
  $old_gift_points = $points_require_for_this_gift * $values['quantity'];
  $new_gift_points = $points_require_for_this_gift * $updated_quantity;

  if ($user_points_remain + $old_gift_points - $new_gift_points < 0) {
    // Set to false
    $passed = false;
    // Display a message
     wc_add_notice( __( "You do not have enough points to redeem this item.", "woocommerce" ), "error" );
  }
  return $passed;
}
add_filter( 'woocommerce_update_cart_validation', 'update_cart_if_user_has_enough_points', 10, 4 );

$insufficient_point_msg_displayed = false;

// Checking and validating when applying coupon to cart
function apply_coupon_if_user_has_enough_points($unknown_bool, $wc_coupon, $wc_discount) {

  global $insufficient_point_msg_displayed;

  if (strpos($wc_coupon->get_code(), 'wc_points_redemption') === 0) {

    $user_points_remain = NK1000_Points_Rewards_Manager::calculate_user_cart_points()['points_remain'];

    if ( isset($_POST['wc_points_rewards_apply_discount_amount']) ) {
      if ( $_POST['wc_points_rewards_apply_discount_amount'] > $user_points_remain ) {
        if ( !$insufficient_point_msg_displayed ) {
          wc_clear_notices();
          wc_add_notice( __( "You do not have enough points to redeem this item.", "woocommerce" ), "error" );
          $insufficient_point_msg_displayed = true;
        }
        return false;
      }
    // } elseif (WC()->session->get( 'wc_points_rewards_discount_amount' ) > $user_points_remain) {
    //   xd(WC()->session->get( 'wc_points_rewards_discount_amount'));
    //   if ( !$insufficient_point_msg_displayed ) {
    //     wc_clear_notices();
    //     wc_add_notice( __( "2You do not have enough points to redeem this item.", "woocommerce" ), "error" );
    //     $insufficient_point_msg_displayed = true;
    //   }
    //   return false;
    }
  }
  return true;
}
add_filter( 'woocommerce_coupon_is_valid', 'apply_coupon_if_user_has_enough_points', 10, 3 );

/**
 * Redirect to shop after login.
 *
 * @param $redirect
 * @param $user
 *
 * @return false|string
 */
function iconic_login_redirect( $redirect, $user ) {

  // Update 'last_logged_in_country' user field
  $location = WC_Geolocation::geolocate_ip();
  update_field('last_logged_in_country', $location['country'], 'user_' . $user->ID);

    $redirect_page_id = url_to_postid( $redirect );
    $checkout_page_id = wc_get_page_id( 'cart' );
    
    if( $redirect_page_id == $checkout_page_id ) {
        return $redirect;
    }
 
    return wc_get_page_permalink( 'home' );
}
 
add_filter( 'woocommerce_login_redirect', 'iconic_login_redirect', 10, 2 );
// END: NK1000_Points_Rewards_Manager

// START: Storefront handheld bottom menu bar
add_filter( 'storefront_handheld_footer_bar_links', 'add_language_switcher' );
function add_language_switcher( $links ) {
  $links['lang'] = array(
    'priority' => 25,
    'callback' => 'storefront_handheld_footer_bar_language',
  );
  return $links;
}

if ( ! function_exists( 'storefront_handheld_footer_bar_language' ) ) {
  /**
   * The language switcher callback function for the handheld footer bar
   *
   * @since 2.0.0
   */
  function storefront_handheld_footer_bar_language() {
    echo '<a href="#">' . esc_attr__( 'Language', 'storefront' ) . '</a>';
    ?><div class="lang-switcher">
      <ul class="columns-3">
        <?php
        $wpml_languages = apply_filters( 'wpml_active_languages', NULL);
        foreach ($wpml_languages as $this_lang_code => $this_lang) { ?>
          <li><a href="<?php echo $this_lang['url']; ?>"><?php echo $this_lang['native_name']; ?></a></li>
        <?php } ?>
      </ul>
    </div><?php
    ?>
    <script>
      $('.storefront-handheld-footer-bar .lang').on('click', function(event) {
        // event.preventDefault();
        $(this).toggleClass('active');
      });
    </script>
    <style>
      .storefront-handheld-footer-bar ul li.lang .lang-switcher {
        position: absolute;
        bottom: -2em;
        left: 0;
        right: 0;
        -webkit-transition: all,ease,.2s;
        transition: all,ease,.2s;
        padding: 1em;
        z-index: 1;
        display: block;
        background-color: white;
      }
      .storefront-handheld-footer-bar ul li.lang > a::before {
        /*content: "\f57e";*/
        /*content: "\f57d";*/
        content: "\f0ac";
      }
      .storefront-handheld-footer-bar ul li.lang.active .lang-switcher {
        bottom: 100%;
      }
      .lang-switcher ul li {
        width: 33.33333% !important;
      }
      .lang-switcher ul li a {
        text-indent: 0;
        height: auto;
        padding: 1em;
        font-weight: bold;
      }
    </style>
    <?php
  }
}
// END: Storefront handheld bottom menu bar

// START: Change Payment Gateway icon and description
add_filter('woocommerce_gateway_icon', 'change_gateway_icon', 10, 2);
function change_gateway_icon ($icon, $id) {
  switch ($id) {
    case 'paydollar':
      return '<img src="/wp-content/themes/nk1000/assets/images/payment_icon_paydollar.png">';
      break;
    case 'ppec_paypal':
      return '<img src="/wp-content/themes/nk1000/assets/images/payment_icon_paypals.png">';
      break;
    default:
      return $icon;
      break;
  }
}
add_filter('woocommerce_gateway_description', 'change_gateway_description', 10, 2);
function change_gateway_description ($description, $id) {
  switch ($id) {
    case 'ppec_paypal':
      switch (ICL_LANGUAGE_CODE) {
        case 'en':
          return 'Pay via PayPal';
          break;
        case 'sc':
          return '经 PayPal 安全付款';
          break;
        case 'tc':
          return '經 PayPal 安全付款';
          break;
        
        default:
          return 'Pay via PayPal';
          break;
      }
      break;
    default:
      return $description;
      break;
  }
}
// END: Change Payment Gateway icon and description

// START: Disable password hint
add_filter('password_hint', 'nk1000_password_hint');
function nk1000_password_hint () {
  switch (ICL_LANGUAGE_CODE) {
    case 'en':
      return 'Hint: The password should be at least 8 characters long';
      break;
    case 'sc':
      return '提示：密码必须设定不少於8个字串';
      break;
    case 'tc':
      return '提示：密碼必須設定不少於8個字串';
      break;
    
    default:
      return '';
      break;
  }
}
// END: Disable password hint

// START: New Account Email Subject for non-HK customer
add_filter('woocommerce_email_subject_customer_new_account', 'new_account_non_hk', 10, 2);
add_filter('woocommerce_email_heading_customer_new_account', 'new_account_non_hk', 10, 2);
function new_account_non_hk ($string, $object) {
  $location = WC_Geolocation::geolocate_ip();
  if ($location['country'] !== 'HK') {
    return __( 'Thanks for Joining and Shopping Coupon from NK1000' );
  }
  return $string;
}
// END: New Account Email Subject for non-HK customer

// START: woocommerce_cart_totals_before_shipping
add_action('woocommerce_cart_totals_before_shipping', 'add_shipping_instruction');
function add_shipping_instruction () {
  _e('<tr>
  <th>Mainland China</th>
  <td data-title="Mainland China">Delivery fee <u>HKD 100</u> to Mainland China, 7 &ndash; 14 business days, with tracking code<br /><strong>(FREE SHIPPING for total amount over HKD 1200)</strong></td>
</tr>
<tr>
  <th>Other Countries</th>
  <td data-title="Other Countries">Shipping fees, calculated based on total weight of shipment, to all eligible countries, 7-14 business days, with tracking code</td>
</tr>
<tr>
  <th>Local Delivery</th>
  <td data-title="Local Delivery">HKD$30 (FREE DELIVERY for total amount over HKD$1200)</td>
</tr>');
}
// END: woocommerce_cart_totals_before_shipping

// START: Tackle country order
add_filter( 'woocommerce_countries',  'handsome_bearded_guy_add_my_country' );
function handsome_bearded_guy_add_my_country( $countries ) {
  $popup_elements = array(
    'HK' => $countries['HK'],
    'CN' => $countries['CN'],
  );
  unset($countries['HK']);
  unset($countries['CN']);
  $countries = array_merge($popup_elements, $countries);
  // xd($countries);
  return $countries;
}
add_filter( 'woocommerce_sort_countries', 'disable_countries_sort' );
function disable_countries_sort () {
  return false;
}
// END: Tackle country order

// START: Tackle Phone Field
// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
  $fields['billing']['billing_phone_country'] = array(
    'label'         => __('Phone', 'woocommerce'),
    'placeholder'   => __('Country ', 'woocommerce'),
    'required'      => false,
    'class'         => array('form-row-one'),
    'clear'         => false,
    'priority' => 98
  );
  $fields['billing']['billing_phone_area'] = array(
    'label'         => __(' ', 'woocommerce'),
    'placeholder'   => _x('Area', 'placeholder', 'woocommerce'),
    'required'      => false,
    'class'         => array('form-row-two'),
    'clear'         => false,
    'priority' => 99
  );
  $fields['billing']['billing_phone']['label'] = ' ';
  $fields['billing']['billing_phone']['class'][0] = 'form-row-three';
  $fields['billing']['billing_phone']['placeholder'] = __('Phone', 'woocommerce');

  return $fields;
}

/**
 * Display field value on the order edit page
 */
 
add_action( 'woocommerce_admin_order_data_after_shipping_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('Phone Country').':</strong> ' . get_post_meta( $order->get_id(), '_billing_phone_country', true ) . '</p>';
    echo '<p><strong>'.__('Phone Area').':</strong> ' . get_post_meta( $order->get_id(), '_billing_phone_area', true ) . '</p>';
}
// END: Tackle Phone Field

// START: Disable WC defer email sent
/**
 * Code goes in theme functions.php.
 */
add_filter( 'woocommerce_defer_transactional_emails', '__return_false' );
// END: Disable WC defer email sent

// START: Deduct point for gift redeem
add_filter( 'wc_points_rewards_decrease_points', 'points_redeem_gift', 10, 5 );
function points_redeem_gift($points, $user_id, $event_type, $data, $order_id) {
  if ($event_type == 'order-redeem') {
    // Log current user total points, for future call of NK1000_Points_Rewards_Manager::calculate_user_order_points
    $current_user_total = WC_Points_Rewards_Manager::get_users_points( $user_id );
    update_post_meta( $order_id, '_wc_points_current_user_total', $current_user_total );
    $points += NK1000_Points_Rewards_Manager::calculate_user_order_points( $order_id )['points_used_gift'];
  }
  return $points;
}
// END: Deduct point for gift redeem

// START: Customer Processing Order Email
add_action('woocommerce_email_customer_details', 'nk100_additional_email_info', 100, 4);
function nk100_additional_email_info( $order, $sent_to_admin, $plain_text, $email ) {
  $order_data = $order->get_data();
  $order_points = NK1000_Points_Rewards_Manager::calculate_user_order_points( $order_data['id'] );
  // xd($order_points);
?>
<h2><?php _e('Bonus Point'); ?></h2>
<div style="margin-bottom: 40px;">
  <table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
    <thead>
      <tr>
        <th class="td" scope="col" style="text-align:left;"><?php esc_html_e( 'Selected Bonus Point Items', 'woocommerce' ); ?></th>
        <th class="td" scope="col" style="text-align:left;"><?php esc_html_e( 'Quantity', 'woocommerce' ); ?></th>
        <th class="td" scope="col" style="text-align:left;"><?php esc_html_e( 'Required', 'woocommerce' ); ?></th>
        <th class="td" scope="col" style="text-align:left;"><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?></th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($order_points['points_used_gift_detail'] as $this_gift) : ?>
      <tr>
        <td><?php echo $this_gift['name']; ?></td>
        <td><?php echo $this_gift['quantity']; ?></td>
        <td><?php echo $this_gift['points_required']; ?></td>
        <td><?php echo $this_gift['subtotal']; ?></td>
      </tr>
      <?php endforeach; ?>
      <?php if ($order_points['points_used_cash'] > 0) : ?>
      <tr>
        <td><?php _e('Points redeem for discount'); ?></td>
        <td>1</td>
        <td><?php echo $order_points['points_used_cash']; ?></td>
        <td><?php echo $order_points['points_used_cash']; ?></td>
      </tr>
      <?php endif; ?>
    </tbody>
    <tfoot>
      <tr>
        <th class="td" scope="row" colspan="3" style="text-align:right;"><?php _e('Total'); ?>:</th>
        <td class="td" ><?php echo $order_points['points_used_total']; ?></td>
      </tr>
    </tfoot>
  </table>
</div>

<h2><?php _e('Bonus Point Balance'); ?></h2>
<div style="margin-bottom: 40px;">
  <table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
    <tbody>
      <tr>
        <td><?php _e('Rewards Previous Balance:'); ?></td>
        <td><?php echo $order_points['points_total']; ?></td>
      </tr>
      <tr>
        <td><?php _e('Rewards Redeemed:'); ?></td>
        <td><?php echo $order_points['points_used_total']; ?></td>
      </tr>
      <tr>
        <td><?php _e('Rewards Earned:'); ?></td>
        <td><?php echo $order_points['points_earned']; ?></td>
      </tr>
      <tr style="border-top: 2px solid black;">
        <td><?php _e('Rewards Balance:'); ?></td>
        <td><?php echo $order_points['points_remain']; ?></td>
      </tr>
    </tbody>
  </table>
</div>
<?php
}
// END: Customer Processing Order Email

// START: Registration Form
function wooc_extra_register_fields() {?>
  <p class="form-row form-row-first">
    <label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?><span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
  </p>
  <p class="form-row form-row-last">
    <label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?><span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
  </p>
  <div class="clear"></div>
  <?php
}
add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );

/**
* register fields Validating.
**/
function wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {

  if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
    $validation_errors->add( 'billing_first_name_error', __( 'First name is required!', 'woocommerce' ) );
  }

  if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
    $validation_errors->add( 'billing_last_name_error', __( 'Last name is required!.', 'woocommerce' ) );
  }

  return $validation_errors;
}
add_action( 'woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3 );
/**
* Below code save extra fields.
*/
function wooc_save_extra_register_fields( $customer_id ) {
  if ( isset( $_POST['billing_first_name'] ) ) {
    //First name field which is by default
    update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
    // First name field which is used in WooCommerce
    update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
  }
  if ( isset( $_POST['billing_last_name'] ) ) {
    // Last name field which is by default
    update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
    // Last name field which is used in WooCommerce
    update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
  }
  if ( isset( $_POST['billing_first_name'] ) && isset( $_POST['billing_last_name'] ) ) {
    // First/Last name as display name
    wp_update_user( array( 'ID' => $customer_id, 'display_name' => sanitize_text_field( $_POST['billing_first_name'] . ' ' . $_POST['billing_last_name'] ) ) );
  }
}
add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );
// END: Registration Form

// START: BCC WC Emails
/**
 * Function adds a BCC header to emails that match our array
 * 
 * @param string $headers The default headers being used
 * @param string $object  The email type/object that is being processed
 */
function add_bcc_to_certain_emails( $headers, $object ) {
  // email types/objects to add bcc to
  $add_bcc_to = array(
    'customer_renewal_invoice',   // Renewal invoice from WooCommerce Subscriptions
    'customer_processing_order',  // Customer Processing order from WooCommerce
    'new_order',
    'customer_invoice',
    );
  // if our email object is in our array
  if ( in_array( $object, $add_bcc_to ) ) {
    // change our headers
    $headers = array( 
      $headers,
      'Bcc: Cherry <cherry@nipponkendai.com>' ."\r\n",
      'Bcc: Suki <suki@nipponkendai.com>' ."\r\n",
      );
  }
  return $headers;
}
add_filter( 'woocommerce_email_headers', 'add_bcc_to_certain_emails', 10, 2 );
// END: BCC WC Emails

// START: Append copy to specific cart items
function wc_cart_item_name_hyperlink( $product_name_link, $cart_item, $cart_item_key ) {
  if ( strpos($cart_item['data']->get_slug(), 'mikei-red-reishi') !== FALSE ) {
    $product_name_link = $product_name_link . '<br>' . __('Capacity: Twin Pack (60 caps x 2) FREE 20 caps');
  }
  if ( strpos($cart_item['data']->get_slug(), 'nissan-reishi') !== FALSE ) {
    $product_name_link = $product_name_link . '<br>' . __('Capacity: 50 packets (2 tablets per packet) / box﻿ FREE 10 packets') . '<br>' . __('Every 4-box order FREE extra 10 packets');
  }
  // 'nissan-reishi';
  // 'mikei-red-reishi';
  // 'Capacity: Twin Pack (60 caps x 2) FREE 20 caps';
  return $product_name_link;
  // return 'asdf';
}
add_filter( 'woocommerce_cart_item_name', 'wc_cart_item_name_hyperlink', 100, 3 );
// END: Append copy to specific cart items

// START: Add pop up in homepage
function add_pop_up() { ?>
  <div id="test-popup" class="white-popup mfp-hide">
    <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
      If you are an existing user who are visiting this revamped site for the first time, please click "<a href="/my-account">Lost your password?</a>" to update your password, thank you.
    <?php elseif (ICL_LANGUAGE_CODE == 'tc') : ?>
      由於網站更新關係，第一次瀏覽此新網站的現有用戶請按"<a href="/tc/my-account/">忘記您的密碼？</a>"以更新密碼。
    <?php elseif (ICL_LANGUAGE_CODE == 'sc') : ?>
      由於网站更新，第一次浏览此新网站的现有用户请按"<a href="/sc/my-account">忘记您的密码？</a>"以更新密码。
    <?php endif; ?>
  </div>
  <style>
    #test-popup {
      max-width: 600px;
      margin: 0 auto;
      background: white;
      border: 10px solid #96588a;
      padding: 80px;
      position: relative;
    }
  </style>
<?php }
add_action('homepage', 'add_pop_up', 80);
// END: Add pop up in homepage

function add_text_below_login_form() {
  if (ICL_LANGUAGE_CODE == 'en'):
    echo  'If you are an existing user who are visiting this revamped site for the first time, please click "<span style="color: #96588a;">Lost your password?</span>" to update your password, thank you.';
  elseif (ICL_LANGUAGE_CODE == 'tc'):
    echo '由於網站更新關係，第一次瀏覽此新網站的現有用戶請按"<span style="color: #96588a;">忘記您的密碼？</span>"以更新密碼。';
  elseif (ICL_LANGUAGE_CODE == 'sc'):
    echo '由於网站更新，第一次浏览此新网站的现有用户请按"<span style="color: #96588a;">忘记您的密码？</span>"以更新密码。';
  endif;
}
add_action('woocommerce_login_form_end', 'add_text_below_login_form');