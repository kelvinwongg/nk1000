<?php
// 
// functions/enqueue.php
// 
if ( ! function_exists( 'alizila_scripts' ) ) {
/**
 * Basic theme CSS and Javascripts
 *
 * @param array $args
 * @return array
 */
  function alizila_scripts() {

    if (WP_DEBUG) {
      $version = date("YmdHis", time());
    } else {
      $version = 'version';
    }

    if (WP_DEBUG || TRUE) :
      /* Storefront Child Theme CSS / JS */
      wp_register_style( 'storefront-child-theme-style', get_stylesheet_directory_uri() . '/assets/css/nk1000.css', array( "foundation", "slick-theme", "magnific-popup", "storefront-style" ), $version );
      wp_enqueue_style( 'storefront-child-theme-style' );
      wp_register_script( 'storefront-child-theme-script', get_stylesheet_directory_uri() . '/assets/js/nk1000.js', array( "jquery", "foundation", "slick", "magnific-popup" ), $version, TRUE );
      wp_enqueue_script( 'storefront-child-theme-script' );
      
      /* Foundation */
      wp_register_style( 'foundation', get_stylesheet_directory_uri() . '/assets/library/foundation-6.4.2-custom/css/foundation.min.css', array(), '6.4.2' );
      wp_enqueue_style( 'foundation' );
      wp_register_script( 'foundation-what-input', get_stylesheet_directory_uri() . '/assets/library/foundation-6.4.2-custom/js/vendor/what-input.js', array( "jquery" ), '4.2.0', TRUE  );
      wp_enqueue_script( 'foundation-what-input' );
      wp_register_script( 'foundation', get_stylesheet_directory_uri() . '/assets/library/foundation-6.4.2-custom/js/vendor/foundation.min.js', array( "jquery", "foundation-what-input" ), '6.4.2', TRUE  );
      wp_enqueue_script( 'foundation' );

      /* Font Awesome */
      // wp_register_script( 'fontawesome', get_template_directory_uri() . '/library/fontawesome-free-5.0.6/on-server/js/fontawesome-all.min.js', array(), '5.0.6', TRUE  );
      // wp_enqueue_script( 'fontawesome' );

      /* Owl Carousel */
      // wp_register_style( 'owl-carousel', get_template_directory_uri() . '/library/owlcarousel/assets/owl.carousel.min.css', array(), '2.2.1' );
      // wp_enqueue_style( 'owl-carousel' );
      // wp_register_style( 'owl-theme', get_template_directory_uri() . '/library/owlcarousel/assets/owl.theme.default.min.css', array( "owl-carousel" ), '2.2.1' );
      // wp_enqueue_style( 'owl-theme' );
      // wp_register_style( 'owl-animate', get_template_directory_uri() . '/library/owlcarousel/assets/animate.css', array( "owl-carousel" ), '3.5.2' );
      // wp_register_script( 'owl-carousel', get_template_directory_uri() . '/library/owlcarousel/owl.carousel.min.js', array( "jquery" ), '2.2.1', TRUE  );
      // wp_enqueue_script( 'owl-carousel' );

      /* Slick */
      wp_register_style( 'slick', get_stylesheet_directory_uri() . '/assets/library/slick-1.8.0/slick/slick.css', array(), '1.8.0' );
      wp_enqueue_style( 'slick' );
      wp_register_style( 'slick-theme', get_stylesheet_directory_uri() . '/assets/library/slick-1.8.0/slick/slick-theme.css', array( "slick" ), '1.8.0' );
      wp_enqueue_style( 'slick-theme' );
      wp_register_script( 'slick', get_stylesheet_directory_uri() . '/assets/library/slick-1.8.0/slick/slick.min.js', array( "jquery" ), '1.8.0', TRUE  );
      wp_enqueue_script( 'slick' );
      
      /* lazySizes */
      // wp_register_script( 'lazysizes', get_template_directory_uri() . '/library/lazysizes/lazysizes.min.js', array(), '4.0.2', TRUE  );
      // wp_enqueue_script( 'lazysizes' );
      // wp_register_script( 'lazysizes-unveilhooks', get_template_directory_uri() . '/library/lazysizes/ls.unveilhooks.min.js', array( 'lazysizes' ), '4.0.2', TRUE  );
      // wp_enqueue_script( 'lazysizes-unveilhooks' );
      
      /* Magnific Popup */
      wp_register_style( 'magnific-popup', get_stylesheet_directory_uri() . '/assets/library/magnific/magnific-popup.css', array(), '1.1.0' );
      wp_enqueue_style( 'magnific-popup' );
      wp_register_script( 'magnific-popup', get_stylesheet_directory_uri() . '/assets/library/magnific/jquery.magnific-popup.min.js', array( "jquery" ), '1.1.0', TRUE  );
      wp_enqueue_script( 'magnific-popup' );
      
      /* Isotope */
      // wp_register_script( 'isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array( "jquery" ), $version, TRUE  );
      // wp_enqueue_script( 'isotope' );
      
      /* jQuery */
      wp_deregister_script( 'jquery' );
      wp_enqueue_script( 'jquery', get_stylesheet_directory_uri() . '/assets/library/jquery/jquery-3.3.1.min.js', array(), '3.3.1', TRUE );
      wp_enqueue_script( 'jquery' );
    endif;
  }
  add_action( 'wp_enqueue_scripts', 'alizila_scripts' );
}