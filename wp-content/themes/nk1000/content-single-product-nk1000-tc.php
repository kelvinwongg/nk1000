<?php //xd(get_field('use_mikei_template')); ?>
<style>
#content.site-content > .col-full {
  background-color: #ddd;
  padding: 0;
  margin: 0;
  max-width: 5000px;
}
#content.site-content > .col-full > .woocommerce {
  max-width: 66.4989378333em;
  margin-left: auto;
  margin-right: auto;
  padding: 0 2.617924em;
  box-sizing: content-box;
}

/* Foundation 6 Media Queries START */
/* Small only */
@media screen and (max-width: 39.9375em) {}
/* Medium and up */
@media screen and (min-width: 40em) {}
/* Medium only */
@media screen and (min-width: 40em) and (max-width: 63.9375em) {}
/* Large and up */
@media screen and (min-width: 64em) {}
/* Large only */
@media screen and (min-width: 64em) and (max-width: 74.9375em) {}
/* Foundation 6 Media Queries END */

/*GENERAL START*/
/*.text-center { text-align: center; }
.text-right { text-align: right; }*/
img { display: inline-block; }
.font-serif { font-family: serif; }
.font-black { color: black; }
.font-white { color: white; }
.font-gold { color: rgb(124, 46, 6); }
.font-bold { font-weight: bold; }
.font-wider { letter-spacing: .2em; }

.bg-white { background-color: white; }
.bg-red-lighter { background-color: rgb(153,44,3); }
.bg-red-darker { background-color: rgb(92,26,0); }
.bg-yellow-pale { background-color: rgb(247,240,214); }

.margin-0 { margin: 0; }
.margin-top-1em { margin-top: 1em; }
.margin-bottom-1em { margin-bottom: 1em; }
.margin-top-2em { margin-top: 2em; }
.margin-bottom-2em { margin-bottom: 2em; }

.padding-0 { padding: 0; }
.padding-top-4em { padding-top: 4em; }
.padding-bottom-4em { padding-bottom: 4em; }
.padding-top-5em { padding-top: 5em; }
.padding-bottom-5em { padding-bottom: 5em; }
.padding-top-bottom-2em { padding-top: 2em; padding-bottom: 2em; }
/*GENERAL END*/

/*START: SECTION HERO*/
.section.hero {
  /*height: 1200px;*/
  background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/HIGHRES_banner_web.jpg');
  background-repeat: no-repeat;
  background-position: center top;
  padding-top: 4em;
  background-color: rgb(248, 228, 209);
}
.section.hero .product-tagline h1 { margin-bottom: .3em; }
.section.hero .product-tagline-2nd { font-size: 1.5em; }
.section.hero .features-highlights { padding-top: 2em; }
.section.hero .features-highlights > .grid-x > .cell { position: relative; }
.section.hero .features-highlights > .grid-x > .cell .overlay {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgb(0,0,0);
  background: -webkit-linear-gradient(bottom, rgba(0,0,0,1) 0%, rgba(255,255,255,0) 38%, rgba(255,255,255,0) 100%);
  background: -o-linear-gradient(bottom, rgba(0,0,0,1) 0%, rgba(255,255,255,0) 38%, rgba(255,255,255,0) 100%);
  background: linear-gradient(to top, rgba(0,0,0,1) 0%, rgba(255,255,255,0) 38%, rgba(255,255,255,0) 100%);
}
.section.hero .features-highlights > .grid-x > .cell .text {
  padding: 2em;
  position: absolute;
  bottom: 0;
  width: 100%;
  font-size: 1.5em;
  color: white;
}
.section.hero .ta {
  padding: 3em 0em;
}
/* Large and up */
@media screen and (min-width: 64em) {
  .section.hero .ta {
    padding: 3em 5em;
  }
}
.section.hero .ta .title {
  font-size: 1.5em;
  font-weight: bold;
  margin-bottom: 2em;
}
.section.hero .ta .text {
  padding: 1em;
  background-color: white;
  font-size: .9em;
  /*letter-spacing: .3em;*/
}
/* Large and up */
@media screen and (min-width: 64em) {
  .section.hero .ta .text {
    font-size: 1.2em;
  }
}
/*END: SECTION HERO*/

/*START: SECTION PURCHASE*/
.section.purchase {
  padding-top: 4em;
  padding-bottom: 4em;
  background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/npk-redresi-pdtpage-v6-07.png');
  background-repeat: no-repeat;
  background-position: center top;
  background-size: cover;
}
.section.purchase .add-to-cart-button .quantity {
  display: inline-block;
  margin-right: 10px;
  vertical-align: top;
}
.section.purchase .add-to-cart-button .quantity input {
  margin-bottom: 0;
}
.section.purchase .add-to-cart-button button {
  margin-bottom: 0;
  height: 40px;
  background-color: rgb(90, 90, 90);
  color: white;
}
/* Large and up */
@media screen and (min-width: 64em) {
  .section.purchase img {
    /*width: 100%;*/
  }
}
/*END: SECTION PURCHASE*/

/*START: SECTION PRODUCT-SPEC*/
.section.product-spec {}
.section.product-spec .product-spec-buttons .accordion-trigger .overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: rgba(0,0,0);
    opacity: .2;
    transition: 0.4s;
}
.section.product-spec .product-spec-buttons .accordion-trigger.active .overlay,
.section.product-spec .product-spec-buttons .accordion-trigger:hover .overlay {
    opacity: 0;
}
.section.product-spec .product-spec-expandable {}
.section.product-spec .product-spec-expandable .product-spec-content {
    /*padding: 0 18px;*/
    /*background-color: white;*/
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
}
.section.product-spec .product-spec-expandable .product-spec-content p {
  max-width: 900px;
  margin: 0 auto;
}
.section.product-spec .product-spec-buttons .button {
  font-size: 2em;
  border-radius: 12px;
  box-shadow: 0 0 13px 4px #555;
  /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#c59b6b+0,e4ce95+25,c59b6b+50,e4ce95+75,c59b6b+100 */
  background: rgb(197,155,107); /* Old browsers */
  background: -moz-linear-gradient(45deg, rgba(197,155,107,1) 0%, rgba(228,206,149,1) 25%, rgba(197,155,107,1) 50%, rgba(228,206,149,1) 75%, rgba(197,155,107,1) 100%); /* FF3.6-15 */
  background: -webkit-linear-gradient(45deg, rgba(197,155,107,1) 0%,rgba(228,206,149,1) 25%,rgba(197,155,107,1) 50%,rgba(228,206,149,1) 75%,rgba(197,155,107,1) 100%); /* Chrome10-25,Safari5.1-6 */
  background: linear-gradient(45deg, rgba(197,155,107,1) 0%,rgba(228,206,149,1) 25%,rgba(197,155,107,1) 50%,rgba(228,206,149,1) 75%,rgba(197,155,107,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c59b6b', endColorstr='#c59b6b',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}
.section.product-spec .product-spec-buttons .text {
  text-shadow: 2px 2px 1px #000000;
  font-size: 1.3em;
}
/*DESKTOP START*/
.section.product-spec .product-spec-buttons .spec-1,
.section.product-spec .product-spec-buttons .spec-2,
.section.product-spec .product-spec-buttons .spec-3 {
  padding-top: 4em;
  padding-bottom: 4em;
  position: relative;
  background-size: auto 100%;
  background-size: cover;
  background-position: right center;
  cursor: pointer;
}
.section.product-spec .product-spec-buttons .spec-1 {
  background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-908633070.jpg');
}
.section.product-spec .product-spec-buttons .spec-2 {
  background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-647577514.jpg');
}
.section.product-spec .product-spec-buttons .spec-3 {
  background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-906813516.jpg');
}
.section.product-spec .product-spec-buttons .spec-1 .content,
.section.product-spec .product-spec-buttons .spec-2 .content,
.section.product-spec .product-spec-buttons .spec-3 .content {
  max-width: 19em;
  color: white;
}
.section.product-spec .product-spec-buttons .spec-1 .content { margin: 0 auto; }
.section.product-spec .product-spec-buttons .spec-2 .content { margin: 0 auto; }
.section.product-spec .product-spec-buttons .spec-3 .content { margin: 0 auto; }
/* Large and up */
@media screen and (min-width: 64em) {
  .section.product-spec .product-spec-buttons .spec-1 .content { float: right; margin-right: 4em; }
  .section.product-spec .product-spec-buttons .spec-3 .content { float: left;  margin-left: 4em; }
}
/*DESKTOP END*/
/*END: SECTION PRODUCT-SPEC*/

/*START: SECTION FAQ*/
.section.faq .accordion {
  background-color: transparent;
}
.section.faq .accordion .accordion-title {
  border: 0;
  border-bottom: 1px solid brown;
}
.section.faq .accordion .accordion-title::before {
  color: brown;
  margin-top: -1rem;
  font-size: 3em;
}
.section.faq .accordion .accordion-content {
  background-color: transparent;
}
/*END: SECTION FAQ*/
</style>

<div class="section hero">
  <div class="col-full">
    <div class="grid-x align-middle">
      <div class="small-12 large-5 cell">
        <div class="product-shot text-center">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/product-with-reflection-tc.png" alt="PRODUCT%20SHOT">
        </div>
      </div>
      <div class="small-12 large-7 cell text-center">
        <div class="product-logo">
          <div class="grid-x">
            <div class="small-6 small-offset-3 cell">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Mikei-logo_horizontal.png" alt="">
            </div>
          </div>
        </div>
        <div class="product-tagline">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/npk-redresi-pdtpage-v6-17.png" alt="">
        </div>
        <div class="product-tagline-2nd">
          <h4 class="font-black font-serif"><strong>天然&nbsp;&nbsp;&nbsp;&nbsp;純淨&nbsp;&nbsp;&nbsp;&nbsp;精華</strong></h4>
          <!-- <div>100% 日本製造</div> -->
        </div>
        <div class="buy-icon">
          <div class="text-center large-text-right">
            <a href="#buy-here" data-scrollTo="#buy-here" class="click-to-scroll"><img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/npk-redresi-pdtpage-v6-23.png" alt=""></a>
          </div>
        </div>
      </div>
    </div>
    <div class="features-highlights">
      <div class="grid-x">
        <div class="small-12 medium-4 cell margin-bottom-1em">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-885287786.jpg" alt="">
          <div class="overlay">
            <div class="text text-center font-wider font-serif">增強免疫力</div>
          </div>
        </div>
        <div class="small-12 medium-4 cell margin-bottom-1em">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-625209642.jpg" alt="">
          <div class="overlay">
            <div class="text text-center font-wider font-serif">促進肝排毒</div>
          </div>
        </div>
        <div class="small-12 medium-4 cell margin-bottom-1em">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-670284230.jpg" alt="">
          <div class="overlay">
            <div class="text text-center font-wider font-serif">抗衰老抗氧化</div>
          </div>
        </div>
      </div>
    </div>
    <div class="ta">
      <div class="grid-x grid-margin-x">
        <div class="small-12 cell">
          <div class="title text-center font-wider font-serif font-black font-bold">什麼人最需要赤靈芝？</div>
        </div>
        <div class="small-6 large-3 cell bg-white margin-bottom-1em">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-521815364.jpg" alt="">
          <div class="text text-center font-gold font-serif">面色暗淡無光</div>
        </div>
        <div class="small-6 large-3 cell bg-white margin-bottom-1em">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-947431848.jpg" alt="">
          <div class="text text-center font-gold font-serif">易病易攰</div>
        </div>
        <div class="small-6 large-3 cell bg-white margin-bottom-1em">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-493155910.jpg" alt="">
          <div class="text text-center font-gold font-serif">經常失眠</div>
        </div>
        <div class="small-6 large-3 cell bg-white margin-bottom-1em">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/iStock-663842896.jpg" alt="">
          <div class="text text-center font-gold font-serif">肝功能下降</div>
        </div>
      </div>
    </div>
  </div>
</div> <!-- .section.hero -->

<div class="section purchase" id="buy-here">
  <div class="col-full">
    <div class="grid-x grid-margin-x align-middle">
      <div class="small-12 medium-4 cell"><img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/MikeiBox_wo_japansticker_tilt.png" alt=""></div>
      <div class="small-12 medium-8 cell">
        <div class="title margin-top-1em">
          <h1 class="font-serif font-gold font-bold">日本【御惠牌<small class="font-gold"><sup>®</sup></small>】赤靈芝</h1>
        </div>
        <p class="font-gold">每粒日本【御惠牌®】赤靈芝膠囊含: <br>
          赤靈芝精華200毫克 (精華濃度16.6比1 ，每粒相等於使用3,320毫克日本上等靈芝提煉) <br>
          NPcaps™膠囊 50毫克 (天然全植物膠囊，素食者，糖尿病患者均可安心服用)</p>
        <p class="font-gold margin-0">規格：每粒250毫克（60粒膠囊丸）</p>
        <p class="font-gold">重量：236g</p>
        <h2 style="margin-top: 0;" class="font-gold">HKD $1,180 / 2盒（加送20粒）</h2>
        <p class="font-gold">建議零售價：$820/盒</p>
        <p class="font-gold margin-0">用法：空腹服，以溫水服用</p>
        <div class="grid-x font-gold">
          <div class="shrink cell">用量：</div>
          <div class="auto cell">保健 － 每日1-2粒 <br>體弱 － 每日2-4粒</div>
        </div>
        <div class="add-to-cart-button text-right margin-top-2em">
          <?php
          /**
           * Custom Loop Add to Cart.
           *
           * Template with quantity and ajax.
           */

          if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly.

          global $product;
          ?>

          <?php if ( ! $product->is_in_stock() ) : ?>

           <a href="<?php echo apply_filters( 'out_of_stock_add_to_cart_url', get_permalink( $product->get_id() ) ); ?>" class="button"><?php echo apply_filters( 'out_of_stock_add_to_cart_text', __( 'Read More', 'woocommerce' ) ); ?></a>

          <?php else : ?>

           <?php
           $link = array(
           'url' => '',
           'label' => '',
           'class' => ''
           );

           switch ( $product->get_type() ) {
           case "variable" :
           $link['url'] = apply_filters( 'variable_add_to_cart_url', get_permalink( $product->get_id() ) );
           $link['label'] = apply_filters( 'variable_add_to_cart_text', __( 'Select options', 'woocommerce' ) );
           break;
           case "grouped" :
           $link['url'] = apply_filters( 'grouped_add_to_cart_url', get_permalink( $product->get_id() ) );
           $link['label'] = apply_filters( 'grouped_add_to_cart_text', __( 'View options', 'woocommerce' ) );
           break;
           case "external" :
           $link['url'] = apply_filters( 'external_add_to_cart_url', get_permalink( $product->get_id() ) );
           $link['label'] = apply_filters( 'external_add_to_cart_text', __( 'Read More', 'woocommerce' ) );
           break;
           default :
           if ( $product->is_purchasable() ) {
           $link['url'] = apply_filters( 'add_to_cart_url', esc_url( $product->add_to_cart_url() ) );
           $link['label'] = apply_filters( 'add_to_cart_text', __( 'Add to cart', 'woocommerce' ) );
           $link['class'] = apply_filters( 'add_to_cart_class', 'add_to_cart_button' );
           } else {
           $link['url'] = apply_filters( 'not_purchasable_url', get_permalink( $product->get_id() ) );
           $link['label'] = apply_filters( 'not_purchasable_text', __( 'Read More', 'woocommerce' ) );
           }
           break;
           }

           // If there is a simple product.
           if ( $product->get_type() == 'simple' ) {
           ?>
           <form action="<?php echo esc_url( $product->add_to_cart_url() ); ?>" class="cart" method="post" enctype="multipart/form-data">
           <?php
           // Displays the quantity box.
           woocommerce_quantity_input();

           // Display the submit button.
           echo sprintf( '<button type="submit" data-product_id="%s" data-product_sku="%s" data-quantity="1" class="%s button product_type_simple">%s</button>', esc_attr( $product->get_id() ), esc_attr( $product->get_sku() ), esc_attr( $link['class'] ), esc_html( $link['label'] ) );
           ?>
           </form>
           <?php
           } else {
           echo apply_filters( 'woocommerce_loop_add_to_cart_link', sprintf('<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="%s button product_type_%s">%s</a>', esc_url( $link['url'] ), esc_attr( $product->get_id() ), esc_attr( $product->get_sku() ), esc_attr( $link['class'] ), esc_attr( $product->get_type() ), esc_html( $link['label'] ) ), $product, $link );
           }

           ?>

          <?php endif; ?>
        </div> <!-- .add-to-cart-button -->
      </div>
    </div>
  </div>
</div> <!-- .section.purchase -->

<div class="section product-spec">

  <div class="desktop show-for-large">

    <div class="product-spec-buttons">
      <div class="grid-x">
        <div class="large-auto cell spec-1 accordion-trigger" data-target="product-spec-content-1">
          <div class="overlay"></div>
          <p class="text-center content">
            <span class="button font-white font-serif">天然</span> <br>
            <span class="text font-bold font-serif">原木培植<br>100%日本製造</span>
          </p>
        </div>
        <div class="large-4 cell spec-2 accordion-trigger" data-target="product-spec-content-2">
          <div class="overlay"></div>
          <p class="text-center content">
            <span class="button font-white font-serif">純淨</span> <br>
            <span class="text font-bold font-serif">通過340種<br>農藥測試</span>
          </p>
        </div>
        <div class="large-auto cell spec-3 accordion-trigger" data-target="product-spec-content-3">
          <div class="overlay"></div>
          <p class="text-center content">
            <span class="button font-white font-serif">精華</span> <br>
            <span class="text font-bold font-serif">16.6倍濃縮精華</span>
          </p>
        </div>
      </div>
    </div>
    <div class="product-spec-expandable">
      <div class="product-spec-content product-spec-content-1">
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <h5 class="text-center font-serif font-white font-bold">日本靈芝世家首創無農藥「原木栽培法」</h5>
            <p class="text-center font-white">野生靈芝容易受污染或蟲蟻侵蝕，令其功效大大降低。<strong>【御惠牌&reg;】</strong><br>養生保健食品採用自家培植的優質赤靈芝，為日本著名靈芝世家 - 黛文丸家族所培植。</p>
          </div>
        </div>
        <div class="bg-red-darker">
          <div class="col-full">
            <div class="grid-x grid-margin-x align-middle">
              <div class="small-12 medium-shrink cell">
                <div class="text-center">
                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/4_mayuzumi.jpg" alt="">
                </div>
              </div>
              <div class="small-12 medium-auto cell">
                <h5 class="text-center font-serif font-white">黛文丸社長為「日本靈芝商品協會」的主席，秉承家族傳統，致力栽種最優質的赤靈芝</h5>
              </div>
            </div>
          </div>
        </div>
        <div class="bg-red-lighter padding-top-bottom-2em font-white">
          <div class="col-full">
            <div class="grid-x align-middle">
              <div class="small-12 large-6 cell text-left">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/reishi-comparison-table2.png" alt="">
                <p class="text-left" style="margin-top: 20px; max-width: 500px;">首創天然無農藥「原木栽培法」，使用一級原條橡木培植出藥效成熟，腎形厚身的赤靈芝。所用原材料均通過日本「食環境衛生研究所」340種農藥測試。</p>
              </div>
              <div class="small-12 large-6 cell text-center">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/wood-log-1.png" alt="">
                <p class="text-left" style="margin: 0 auto; margin-top: 20px; max-width: 250px;">利用一級原條橡木作培植，能更接近靈芝天然生長環境，所獲的靈芝子實體亦較大，藥效更成熟。</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="product-spec-content product-spec-content-2">
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <h5 class="text-center font-serif font-white font-bold">香港，日本雙重把關質量保證</h5>
            <div class="text-center">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/No-pesticide-pure-essence.png" alt="">
            </div>
          </div>
        </div>
        <div class="bg-red-darker">
          <div class="col-full padding-top-bottom-2em">
            <p class="text-center font-white">【御惠牌<sup>&reg;</sup>】所選用的赤靈芝，從培植，生產，出口到入口，均經過一系列嚴格安全測試，確保不受任何人為及環境污染，為使用者把關。其中產品所培植農場的水，土壤和原木等原材料，均已通過日本「環境衛生研究所」340種*測試（當中包括農藥，重金屬，輻射污染物和微生物等測試等）;另於產品出口前，更會將每一批製成品再次經日本獨立化驗室JFRL及NKKK重複測試;最後入口香港後，更會通過香港海關及中成藥質量測試化驗，以證安全。</p>
          </div>
        </div>
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <p class="text-center font-white">每一批【御惠牌<sup>&reg;</sup>】赤靈芝均通過日本獨立&ldquo;食環境衛生研究所&rdquo;340項殘餘農藥測試，證實&ldquo;零農藥&rdquo;:</p>
            <p class="text-center"><a href="https://www.nk1000.com/userfiles/file/Pesticide residues test (Lot 1300).pdf" onclick="window.open(this.href,'','resizable=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,fullscreen=no,dependent=no,status'); return false" class="font-white">2016 年 8 月</a></p>
            <p class="text-center"><a href="https://www.nk1000.com/userfiles/file/pesticide%20residues%20test%20(lot%201290).pdf" onclick="window.open(this.href,'','resizable=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,fullscreen=no,dependent=no,status'); return false" class="font-white">2016 年 7 月</a></p>
            <div class="text-center">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Mikei-certs.png" alt="">
            </div>
            <p class="font-white text-center">比香港食安中心&lt;食物內除害劑殘餘規例&gt;蘑菇類別要求的85項為多</p>
          </div>
        </div>
        <div class="bg-red-darker">
          <div class="col-full padding-top-bottom-2em font-white text-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/test-tube-comparison-tc.png" alt=""><br>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Mikei-essence-benefit.jpg" alt="">
          </div>
        </div>
        <div class="bg-red-lighter">
          <div class="grid-x align-center align-middle">
            <div class="shrink cell">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Mikei-finest-powder.png" alt="">
            </div>
            <div class="shrink cell">
              <ul class="font-white">
                <li><span style="">100％溶於水，易吸收</span></li>
                <li><span style="">日本&ldquo;熱水抽出法&rdquo;提煉精華粉末</span></li>
                <li><span style="">靈芝藥味濃郁</span></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="bg-red-darker padding-top-bottom-2em">
          <h5 class="text-center font-serif font-white font-bold">產品認證</h5>
          <p class="font-white text-center">【御惠牌&reg;】赤靈芝的品質已獲日本靈芝商品協會（JRA）認證。日本靈芝商品協會是由日本靈芝培植商和日本靈芝產品製造商組成的國際性非謀利組織。協會致力維護日本靈芝生產之最高質量與標準，保護一般靈芝消費者的利益及健康，並通過多種媒體向消費者介紹靈芝健康食品的最新情報。</p>
          <div class="text-center margin-top-1em">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Japan_Reishi_Asso_logo_black_200x114.jpg" alt="">
          </div>
        </div>
      </div>
      <div class="product-spec-content product-spec-content-3">
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <h5 class="text-center font-serif font-white font-bold">「熱水抽出法」提煉靈芝精華</h5>
            <p class="font-white text-center">【御惠牌&reg;】採用日本提煉技術「熱水抽出法」，濃縮成16.6倍靈芝精華，高度提取有效藥用成份，同時殺菌，剔除雜質;而且其有效成份多醣體，被測定為市面靈芝產品之冠*。</p>
            <p class="font-white text-center">*2000年消委會托浸會大學中醫藥研究所進行的測試，抽取市面上三十二種靈芝產品樣本，包括二十六種靈芝及靈芝孢子成藥，劑型有膠囊，藥片，方塊，散劑及沖劑等。日本<strong>【御惠牌&reg;】</strong>赤靈芝的有效成分多醣體為所有產品中最高。<a class="font-white" href="https://www.nk1000.com/userfiles/file/test report.jpg">{有關消委會測試}</a></p>
            <div class="text-center">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/reishiessence5.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>

  </div> <!-- .desktop -->


  <div class="mobile hide-for-large">

    <div class="product-spec-buttons">
      <div class="grid-x">
        <div class="large-auto cell spec-1 accordion-trigger" data-target="product-spec-content-1" data-multiOpen='true'>
          <div class="overlay"></div>
          <p class="text-center content">
            <span class="button font-white font-serif">天然</span> <br>
            <span class="text font-bold font-serif">原木培植<br>100%日本製造</span>
          </p>
        </div>
      </div>
    </div>
    <div class="product-spec-expandable">
      <div class="product-spec-content product-spec-content-1">
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <h5 class="text-center font-serif font-white font-bold">日本靈芝世家首創無農藥「原木栽培法」</h5>
            <p class="text-center font-white">野生靈芝容易受污染或蟲蟻侵蝕，令其功效大大降低。<strong>【御惠牌&reg;】</strong><br>養生保健食品採用自家培植的優質赤靈芝，為日本著名靈芝世家 - 黛文丸家族所培植。</p>
          </div>
        </div>
        <div class="bg-red-darker">
          <div class="col-full">
            <div class="grid-x grid-margin-x align-middle">
              <div class="small-12 medium-shrink cell">
                <div class="text-center">
                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/4_mayuzumi.jpg" alt="">
                </div>
              </div>
              <div class="small-12 medium-auto cell">
                <h5 class="text-center font-serif font-white">黛文丸社長為「日本靈芝商品協會」的主席，秉承家族傳統，致力栽種最優質的赤靈芝</h5>
              </div>
            </div>
          </div>
        </div>
        <div class="bg-red-lighter padding-top-bottom-2em font-white">
          <div class="col-full">
            <div class="grid-x align-middle">
              <div class="small-12 large-6 cell text-left">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/reishi-comparison-table2.png" alt="">
                <p class="text-left" style="margin-top: 20px; max-width: 500px;">首創天然無農藥「原木栽培法」，使用一級原條橡木培植出藥效成熟，腎形厚身的赤靈芝。所用原材料均通過日本「食環境衛生研究所」340種農藥測試。</p>
              </div>
              <div class="small-12 large-6 cell text-center">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/wood-log-1.png" alt="">
                <p class="text-left" style="margin: 0 auto; margin-top: 20px; max-width: 250px;">利用一級原條橡木作培植，能更接近靈芝天然生長環境，所獲的靈芝子實體亦較大，藥效更成熟。</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="product-spec-buttons">
      <div class="grid-x">
        <div class="large-4 cell spec-2 accordion-trigger" data-target="product-spec-content-2"  data-multiOpen='true'>
          <div class="overlay"></div>
          <p class="text-center content">
            <span class="button font-white font-serif">純淨</span> <br>
            <span class="text font-bold font-serif">通過340種<br>農藥測試</span>
          </p>
        </div>
      </div>
    </div>
    <div class="product-spec-expandable">
      <div class="product-spec-content product-spec-content-2">
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <h5 class="text-center font-serif font-white font-bold">香港，日本雙重把關質量保證</h5>
            <div class="text-center">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/No-pesticide-pure-essence.png" alt="">
            </div>
          </div>
        </div>
        <div class="bg-red-darker">
          <div class="col-full padding-top-bottom-2em">
            <p class="text-center font-white" style="margin: 0 auto; max-width: 550px;">【御惠牌<sup>&reg;</sup>】所選用的赤靈芝，從培植，生產，出口到入口，均經過一系列嚴格安全測試，確保不受任何人為及環境污染，為使用者把關。其中產品所培植農場的水，土壤和原木等原材料，均已通過日本「環境衛生研究所」340種*測試（當中包括農藥，重金屬，輻射污染物和微生物等測試等）;另於產品出口前，更會將每一批製成品再次經日本獨立化驗室JFRL及NKKK重複測試;最後入口香港後，更會通過香港海關及中成藥質量測試化驗，以證安全。</p>
          </div>
        </div>
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <p class="text-center font-white">每一批【御惠牌<sup>&reg;</sup>】赤靈芝均通過日本獨立&ldquo;食環境衛生研究所&rdquo;340項殘餘農藥測試，證實&ldquo;零農藥&rdquo;:</p>
            <p class="text-center"><a href="https://www.nk1000.com/userfiles/file/Pesticide residues test (Lot 1300).pdf" onclick="window.open(this.href,'','resizable=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,fullscreen=no,dependent=no,status'); return false" class="font-white">2016 年 8 月</a></p>
            <p class="text-center"><a href="https://www.nk1000.com/userfiles/file/pesticide%20residues%20test%20(lot%201290).pdf" onclick="window.open(this.href,'','resizable=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,fullscreen=no,dependent=no,status'); return false" class="font-white">2016 年 7 月</a></p>
            <div class="text-center">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Mikei-certs.png" alt="">
            </div>
            <p class="font-white text-center">比香港食安中心&lt;食物內除害劑殘餘規例&gt;蘑菇類別要求的85項為多</p>
          </div>
        </div>
        <div class="bg-red-darker">
          <div class="col-full padding-top-bottom-2em font-white text-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/test-tube-comparison.png" alt=""><br>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Mikei-essence-benefit.jpg" alt="">
          </div>
        </div>
        <div class="bg-red-lighter">
          <div class="grid-x align-center align-middle">
            <div class="shrink cell">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Mikei-finest-powder.png" alt="">
            </div>
            <div class="shrink cell">
              <ul class="font-white">
                <li><span style="">100％溶於水，易吸收</span></li>
                <li><span style="">日本&ldquo;熱水抽出法&rdquo;提煉精華粉末</span></li>
                <li><span style="">靈芝藥味濃郁</span></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="bg-red-darker padding-top-bottom-2em">
          <h5 class="text-center font-serif font-white font-bold">產品認證</h5>
          <p class="font-white text-center">【御惠牌&reg;】赤靈芝的品質已獲日本靈芝商品協會（JRA）認證。日本靈芝商品協會是由日本靈芝培植商和日本靈芝產品製造商組成的國際性非謀利組織。協會致力維護日本靈芝生產之最高質量與標準，保護一般靈芝消費者的利益及健康，並通過多種媒體向消費者介紹靈芝健康食品的最新情報。</p>
          <div class="text-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/Japan_Reishi_Asso_logo_black_200x114.jpg" alt="">
          </div>
        </div>
      </div>
    </div>

    <div class="product-spec-buttons">
      <div class="grid-x">
        <div class="large-auto cell spec-3 accordion-trigger" data-target="product-spec-content-3"  data-multiOpen='true'>
          <div class="overlay"></div>
          <p class="text-center content">
            <span class="button font-white font-serif">精華</span> <br>
            <span class="text font-bold font-serif">16.6倍濃縮精華</span>
          </p>
        </div>
      </div>
    </div>
    <div class="product-spec-expandable">
      <div class="product-spec-content product-spec-content-3">
        <div class="bg-red-lighter">
          <div class="col-full padding-top-bottom-2em">
            <h5 class="text-center font-serif font-white font-bold">「熱水抽出法」提煉靈芝精華</h5>
            <p class="font-white text-center">【御惠牌&reg;】採用日本提煉技術「熱水抽出法」，濃縮成16.6倍靈芝精華，高度提取有效藥用成份，同時殺菌，剔除雜質;而且其有效成份多醣體，被測定為市面靈芝產品之冠*。</p>
            <p class="font-white text-center">*2000年消委會托浸會大學中醫藥研究所進行的測試，抽取市面上三十二種靈芝產品樣本，包括二十六種靈芝及靈芝孢子成藥，劑型有膠囊，藥片，方塊，散劑及沖劑等。日本<strong>【御惠牌&reg;】</strong>赤靈芝的有效成分多醣體為所有產品中最高。<a class="font-white" href="https://www.nk1000.com/userfiles/file/test report.jpg">{有關消委會測試}</a></p>
            <div class="text-center">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/reishiessence5.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>

  </div> <!-- .mobile -->

</div> <!-- .section.product-spec -->

<div class="section faq">
  <div class="bg-yellow-pale padding-top-4em padding-bottom-4em">
    <div class="col-full">
      <h3 class="text-center font-black font-serif font-wider">如何分辨赤靈芝優劣?</h3>
      <div class="grid-x grid-margin-x margin-top-2em padding-bottom-5em">
        <div class="small-6 large-3 cell text-center">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/npk-redresi-pdtpage-v6-03.png" alt="">
          <h5 class="font-serif font-gold font-bold margin-top-1em">藥味純正濃郁</h5>
        </div>
        <div class="small-6 large-3 cell text-center">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/npk-redresi-pdtpage-v6-04.png" alt="">
          <h5 class="font-serif font-gold font-bold margin-top-1em">藥液清晰無雜質</h5>
        </div>
        <div class="small-6 large-3 cell text-center">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/npk-redresi-pdtpage-v6-05.png" alt="">
          <h5 class="font-serif font-gold font-bold margin-top-1em">入口即溶由苦入甘</h5>
        </div>
        <div class="small-6 large-3 cell text-center">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/npk-redresi-pdtpage-v6-06.png" alt="">
          <h5 class="font-serif font-gold font-bold margin-top-1em">質感極緻幼細</h5>
        </div>
      </div>
      <h3 class="text-center font-black font-serif font-wider">逾20年口碑 進駐倫敦Harrods頂級百貨</h3>
      <p class="text-center font-gold" style="max-width: 600px; margin: 0 auto;">【御惠牌®】赤靈芝自開售以來，已熱銷全球15個國家，網絡遍布亞洲，北美，歐洲，近進注英國倫敦歷史悠久名牌集中地 – Harrods 百貨公司，口碑毋庸置疑。</p>
      <div class="text-center">
        <img class="margin-top-2em padding-bottom-4em" src="<?php echo get_stylesheet_directory_uri(); ?>/mekei-assets/har2.jpg" alt="">
      </div>
      <h3 class="text-center font-black font-serif font-wider">FAQ</h3>
      <ul class="accordion" data-accordion data-allow-all-closed="true">
        <li class="accordion-item" data-accordion-item>
          <a href="#" class="accordion-title">
            <h3 class="text-left font-serif font-gold margin-0">1. 何謂赤靈芝？</h3>
          </a>
          <div class="accordion-content" data-tab-content>
            <p>赤靈芝在中國及日本，均被推崇為上藥中之極品。自古以來，中國及日本皇室皆視赤靈芝為長壽健身之補藥。赤靈芝具有提高免疫力及調節身體機能，抗衰老等功效。常服赤靈芝可幫助增強體質，療效顯而易見。</p>
          </div>
        </li>
        <li class="accordion-item" data-accordion-item>
          <a href="#" class="accordion-title">
            <h3 class="text-left font-serif font-gold margin-0">2. 赤靈芝可否與其他藥物一同使用？</h3>
          </a>
          <div class="accordion-content" data-tab-content>
            <p>赤靈芝雖然是保健品，但普遍來說，在服用新的藥物前，我們建議用家先諮詢保健醫生。目前為止，仍沒有任何報告指出赤靈芝不能與其他藥物一起服用。然而，若病人剛經歷器官移植手術或正在服用免疫抑制藥物，應在服用任何免疫調節藥物時（如赤靈芝等）保持注意，並諮詢保健醫生。</p>
          </div>
        </li>
        <li class="accordion-item" data-accordion-item>
          <a href="#" class="accordion-title">
            <h3 class="text-left font-serif font-gold margin-0">3. 是否所有人仕也能安全服用赤靈芝？</h3>
          </a>
          <div class="accordion-content" data-tab-content>
            <p>暫時沒有研究指出任何人仕應該避免服用赤靈芝。然而，我們仍建議所有人仕在服用任何新的保健品時先諮詢保健醫生，尤其是孕婦或在用母乳哺育的母親。</p>
          </div>
        </li>
        <li class="accordion-item" data-accordion-item>
          <a href="#" class="accordion-title">
            <h3 class="text-left font-serif font-gold margin-0">4. 服食赤靈芝會否有副作用？</h3>
          </a>
          <div class="accordion-content" data-tab-content>
            <p>赤靈芝是一種天然物質，並且能夠在沒有副作用下長期服用。有例子是個別敏感人仕曾出現如輕微肚痛、口鼻乾燥、暈眩、疼痛和皮疹的排毒徵兆。這些徵兆會在數天內消失。若持續出現徵兆，請諮詢保健醫生。</p>
          </div>
        </li>
        <li class="accordion-item" data-accordion-item>
          <a href="#" class="accordion-title">
            <h3 class="text-left font-serif font-gold margin-0">5. 服用多久後才能看見顯著效果？</h3>
          </a>
          <div class="accordion-content" data-tab-content>
            <p>每天持續服用高品質赤靈芝後看到顯著效果需時因人而異，約數星期到數月不等，但大部份用家在一個月內便能夠感受到身體上明顯的轉變和成效。</p>
          </div>
        </li>
        <li class="accordion-item" data-accordion-item>
          <a href="#" class="accordion-title">
            <h3 class="text-left font-serif font-gold margin-0">6. 可以持續服食赤靈芝多久？</h3>
          </a>
          <div class="accordion-content" data-tab-content>
            <p>赤靈芝沒有服用時間長短的限制。每天服用赤靈芝是關注身體健康人仕所採取的一個自我保障預防措施。</p>
          </div>
        </li>
        <li class="accordion-item" data-accordion-item>
          <a href="#" class="accordion-title">
            <h3 class="text-left font-serif font-gold margin-0">7. 赤靈芝膠囊是否適合素食者？</h3>
          </a>
          <div class="accordion-content" data-tab-content>
            <p>適合。赤靈芝是純天然和適合各類型有不同飲食習慣的人仕，包括素食者，糖尿病者和有任何忌口的人仕。</p>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<script>
var acc = document.getElementsByClassName("accordion-trigger");
var i;
var panels = document.getElementsByClassName("product-spec-content");

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {

    var targetPanels = document.getElementsByClassName(this.dataset.target);
    
    // Toggle trigger active state
    this.classList.toggle("active");
    
    if ( this.classList.contains("active") ) {
      if (!this.dataset.multiopen) {
        // Close all panels first
        for (j = 0; j < panels.length; j++) {
          panels[j].style.maxHeight = null;
        }
        // Clear Trigger Active state
        for (j = 0; j < acc.length; j++) {
          acc[j].classList.remove("active");
        }
        // Add back the active state
        this.classList.add("active");
      }
      // Open the target panels
      for (k = 0; k < targetPanels.length; k++) {
        targetPanels[k].style.maxHeight = targetPanels[k].scrollHeight + "px";
      }
    } else {
      // Close the target panels
      for (k = 0; k < targetPanels.length; k++) {
        targetPanels[k].style.maxHeight = null;
      }
    }

  });
}
</script>