<?php
// Query Products by Product Category
 $args = array(
    'post_type'             => 'product',
    'post_status'           => 'publish',
    'ignore_sticky_posts'   => 1,
    'posts_per_page'        => '12',
    'tax_query'             => array(
        array(
            'taxonomy'      => 'product_cat',
            'field'         => 'slug', //This is optional, as it defaults to 'term_id'
            'terms'         => 'gifts',
            'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
        ),
        // array(
        //     'taxonomy'      => 'product_visibility',
        //     'field'         => 'slug',
        //     'terms'         => 'exclude-from-catalog', // Possibly 'exclude-from-search' too
        //     'operator'      => 'NOT IN'
        // )
    )
);
$gifts_products = new WP_Query($args);
$user_cart_points = NK1000_Points_Rewards_Manager::calculate_user_cart_points();
?>
<div class="gift_redeem <?php echo (is_user_logged_in())? "logged_in" : ""; ?>">
  
  <h2><?php _e('Point Redeem'); ?></h2>
  
  <?php if (is_user_logged_in()) : ?>
    <table cellspacing="0" class="shop_table shop_table_responsive">
      <tbody>
        <tr>
          <th><?php _e('Total Points'); ?></th>
          <td data-title="Total Points">
            <strong class="total_points"><?php echo $user_cart_points['points_total']; ?></strong>
          </td>
        </tr>
        <tr>
          <th><?php _e('Used Points'); ?></th>
          <td data-title="Used Points">
            <strong class="used_points"><?php echo $user_cart_points['points_used_total']; ?></strong>
          </td>
        </tr>
        <tr>
          <th><?php _e('Remained Points'); ?></th>
          <td data-title="Remained Points">
            <strong class="remained_points"><?php echo $user_cart_points['points_remain']; ?></strong>
          </td>
        </tr>
      </tbody>
    </table>

    <table cellspacing="0" class="shop_table shop_table_responsive">
      <tbody>

      <tr>
        <th style="vertical-align: top; width: 30%;"><?php _e('Cash Dollars'); ?></th>
        <td style="vertical-align: top;" data-title="Cash Dollars">
          <form action="./" method="post">
            <input type="text" min="5" max="<?php echo $user_cart_points['points_total'];?>" name="wc_points_rewards_apply_discount_amount" value="<?php echo $user_cart_points['points_used_cash']; ?>" style="width: 50px;">
            <input type="hidden" name="wc_points_rewards_apply_discount" value="Apply Discount">
            <input type="hidden" name="nk1000_points_rewards_apply_discount" value="Clear Discount">
            <?php if (is_cart()) { ?>
              <input type="submit" value="<?php _e('Add Points', 'storefront_child'); ?>" class="button">
            <?php } else { global $woocommerce; $cart_url = $woocommerce->cart->get_cart_url();?>
              <a href="<?php echo $cart_url; ?>" class="button"><?php _e('Add Points', 'storefront_child'); ?></a>
            <?php } ?>
          </form>
          <br>
          <!-- 1Point = HKD$5<br> -->
          5 <?php _e('points'); ?> = $25 <?php _e('coupon'); ?><br>
          10 <?php _e('points'); ?> = $50 <?php _e('coupon'); ?><br>
          20 <?php _e('points'); ?> = $100 <?php _e('coupon'); ?><br>
          40 <?php _e('points'); ?> = $200 <?php _e('coupon'); ?><br>
          <?php _e('and so on.'); ?>
        </td>
      </tr>
      
      <tr>
        <th><?php _e('Gifts'); ?></th>
        <td data-title="Gifts">
          <select id="gift_select" style="width: 50%; height: 45px; vertical-align: top;">
            <option value=""><?php _e('Please select a gift'); ?></option>
            <?php
            // Loop out the Gifts products
            if ( $gifts_products->have_posts() ) {
              while ( $gifts_products->have_posts() ) {
                $gifts_products->the_post();?>
                  <option
                    value="<?php echo get_the_ID(); ?>"
                    data-product_id="<?php echo get_the_ID(); ?>"
                    data-points="<?php echo get_post_meta(get_the_ID(), '_redeem_points')[0]; ?>"><?php the_title(); ?> (<?php echo get_post_meta(get_the_ID(), '_redeem_points')[0]; ?> <?php _e('points', 'woocommerce'); ?>)</option>
              <?php }
              /* Restore original Post Data */
              wp_reset_postdata();
            } else {
              // no posts found
            }
            ?>
          </select>
          <a
            id="gift_add_to_cart_button"
            href="#"
            data-quantity="1"
            class="button add_to_cart_button"
            data-product_id=""
            data-product_sku=""
            data-points=""
            aria-label="Add gift to your cart"
            rel="nofollow"
            style="vertical-align: top;"><?php _e('Add Gift', 'storefront_child'); ?></a>
        </td>
      </tr>

      </tbody>
    </table>
    <script>
      $(document).ready(function(){
        $('#gift_select').on('change', function() {
          $('#gift_add_to_cart_button').attr('href', '<?php echo apply_filters( 'wpml_home_url', get_option( 'home' ) ) . '/cart/?add-to-cart='; ?>' + $( "#gift_select option:selected" ).data('product_id'))
          $('#gift_add_to_cart_button').attr('data-product_id', $( "#gift_select option:selected" ).data('product_id'))
          $('#gift_add_to_cart_button').attr('data-points', $( "#gift_select option:selected" ).data('points'))
        });
      });
    </script>
  <?php else: ?>
    <p><?php _e('Login to redeem your points', 'storefront_child'); ?></p>
  <?php endif; ?>

</div>