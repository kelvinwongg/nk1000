<?php
/**
 * WooCommerce Points and Rewards
 *
 * @package     WC-Points-Rewards/Templates
 * @author      WooThemes
 * @copyright   Copyright (c) 2013, WooThemes
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * My Account - My Points
 */
?>
<h2><?php printf( __( 'My %s', 'woocommerce-points-and-rewards' ), $points_label  ); ?></h2>

<p><?php printf( __( "You have %d %s", 'woocommerce-points-and-rewards' ), $points_balance, $points_label ); ?></p>

<?php if ( $events ) : ?>
	<table class="shop_table my_account_points_rewards my_account_orders">
		<thead>
			<tr>
				<th class="points-rewards-event-description"><span class="nobr"><?php _e( 'Event', 'woocommerce-points-and-rewards' ); ?></span></th>
				<th class="points-rewards-event-date"><span class="nobr"><?php _e( 'Date', 'woocommerce-points-and-rewards' ); ?></span></th>
				<th class="points-rewards-event-points"><span class="nobr"><?php echo esc_html( $points_label ); ?></span></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ( $events as $event ) : ?>
			<tr class="points-event">
				<td class="points-rewards-event-description">
					<?php echo $event->description; ?>
				</td>
				<td class="points-rewards-event-date">
					<?php echo '<abbr title="' . esc_attr( $event->date_display ) . '">' . esc_html( $event->date_display_human ) . '</abbr>'; ?>
				</td>
				<td class="points-rewards-event-points" width="1%">
					<?php echo ( $event->points > 0 ? '+' : '' ) . $event->points; ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

	<div class="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination">
	<?php if ( $current_page != 1 ) : ?>
		<a class="woocommerce-button woocommerce-button--previous woocommerce-Button woocommerce-Button--previous button" href="<?php echo esc_url( wc_get_endpoint_url( 'points-and-rewards', $current_page - 1 ) ); ?>"><?php _e( 'Previous', 'woocommerce-points-and-rewards' ); ?></a>
	<?php endif; ?>

	<?php if ( $current_page * $count < $total_rows ) : ?>
		<a class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button" href="<?php echo esc_url( wc_get_endpoint_url( 'points-and-rewards', $current_page + 1 ) ); ?>"><?php _e( 'Next', 'woocommerce-points-and-rewards' ); ?></a>
	<?php endif; ?>
	</div>

<?php endif; ?>
<h2><?php printf( __( 'My %s Requirement', 'storefront_child' ), $points_label  ); ?></h2>
<table id="pointsRequirementTable">
	<tbody>
		<tr>
			<th><?php printf( __( '%s Requirement', 'storefront_child' ), $points_label  ); ?></th>
			<th><?php _e( 'Redeemed Cash Value', 'storefront_child' ); ?></th>
			<th><?php _e( 'Gift', 'storefront_child' ); ?></th>
		</tr>
		<tr>
			<td>5</td>
			<td><?php printf( __( 'HKD $%s', 'storefront_child' ), '25'  ); ?></td>
			<td>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bonus-points/bp_ppp.png" alt="">
				<br>
				<?php printf( __( 'Cash Dollars HKD $%s', 'storefront_child' ), '25'  ); ?>
			</td>
		</tr>
		<tr>
			<td>10</td>
			<td><?php printf( __( 'HKD $%s', 'storefront_child' ), '50'  ); ?></td>
			<td>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bonus-points/bp_ppp.png" alt="">
				<br>
				<?php printf( __( 'Cash Dollars HKD $%s', 'storefront_child' ), '50'  ); ?>
			</td>
		</tr>
		<tr>
			<td>20</td>
			<td><?php printf( __( 'HKD $%s', 'storefront_child' ), '100'  ); ?></td>
			<td>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bonus-points/bp_ppp.png" alt="">
				<br>
				<?php printf( __( 'Cash Dollars HKD $%s', 'storefront_child' ), '100'  ); ?>
			</td>
		</tr>
		<tr>
			<td>40</td>
			<td><?php printf( __( 'HKD $%s', 'storefront_child' ), '200'  ); ?></td>
			<td>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bonus-points/bp_ppp.png" alt="">
				<br>
				<?php printf( __( 'Cash Dollars HKD $%s', 'storefront_child' ), '200'  ); ?>
			</td>
		</tr>
		<tr>
			<td>55</td>
			<td>-</td>
			<td>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bonus-points/glagold-60.jpg" alt="">
				<br>
				<?php _e( 'Glagold (60 caps)', 'storefront_child' ); ?>
			</td>
		</tr>
		<tr>
			<td>60</td>
			<td>-</td>
			<td>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bonus-points/mikei.jpg" alt="">
				<br>
				<?php _e( 'Mikei (60 caps)', 'storefront_child' ); ?>
			</td>
		</tr>
		<tr>
			<td>80</td>
			<td>-</td>
			<td>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bonus-points/tenken.jpg" alt="">
				<br>
				<?php _e( 'Tenken (60 caps)', 'storefront_child' ); ?>
			</td>
		</tr>
		<tr>
			<td>125</td>
			<td>-</td>
			<td>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bonus-points/molval.jpg" alt="">
				<br>
				<?php _e( 'Molval Metabolic 850 (120 caps)', 'storefront_child' ); ?>
			</td>
		</tr>
		<tr>
			<td>140</td>
			<td>-</td>
			<td>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bonus-points/ue-1.jpg" alt="">
				<br>
				<?php _e( 'UE-1 (60 caps)', 'storefront_child' ); ?>
			</td>
		</tr>
		<tr>
			<td>250</td>
			<td>-</td>
			<td>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bonus-points/glagold-330.jpg" alt="">
				<br>
				<?php _e( 'Glagold (330 caps)', 'storefront_child' ); ?>
			</td>
		</tr>
	</tbody>
</table>
