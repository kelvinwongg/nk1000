<?php
/**
 * Customer invoice email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-invoice.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates/Emails
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Executes the e-mail header.
 *
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php printf( __( 'Dear %s,', 'woocommerce' ), $order->get_formatted_billing_full_name() ); ?></p>

<?php
$shipping = $order->get_items( 'shipping' );
foreach ($shipping as $key => $this_shipping) {
  $shipping_method_title = $this_shipping->get_method_title();
  $shipping_method_id = $this_shipping->get_method_id();
  break;
}
// xd($shipping_method_title);
// xd($shipping_method_id); // wc_hkp_shipping_id
if ($shipping_method_id == 'wc_hkp_shipping_id') {
	printf( __( '<p>Your order is with HK SpeedPost. You may visit the below link to track the record, and your Tracking code is : %s
	<p><a href="http://www.hongkongpost.hk/en/mail_tracking/index.html">http://www.hongkongpost.hk/en/mail_tracking/index.html</a></p>
	<p>Thank you for your order from NK1000. If you have any questions about your order please contact us <a href="mailto:order@nipponkendai.com">order@nipponkendai.com</a>.</p>', 'woocommerce' ), get_field('shipping_tracking_code') );
}
?>

<?php if ( $order->has_status( 'pending' ) ) : ?>
	<p>
	<?php
	printf(
		wp_kses(
			/* translators: %1s item is the name of the site, %2s is a html link */
			__( 'An order has been created for you on %1$s. %2$s', 'woocommerce' ),
			array(
				'a' => array(
					'href' => array(),
				),
			)
		),
		esc_html( get_bloginfo( 'name', 'display' ) ),
		'<a href="' . esc_url( $order->get_checkout_payment_url() ) . '">' . esc_html__( 'Pay for this order', 'woocommerce' ) . '</a>'
	);
	?>
	</p>
<?php endif; ?>

<?php

/**
 * Hook for the woocommerce_email_order_details.
 *
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * Hook for the woocommerce_email_order_meta.
 *
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/**
 * Hook for woocommerce_email_customer_details.
 *
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * Executes the email footer.
 *
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
