<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php
$user_data = get_user_by('login', $user_login);
$user_display_name = $user_data->first_name . ' ' . $user_data->last_name;
?>
<p><?php printf( __( 'Dear %s', 'woocommerce' ), esc_html( $user_display_name ) ); ?>,</p>

<?php
$location = WC_Geolocation::geolocate_ip();
// xd($location['country']);
if ($location['country'] == 'HK') : ?>
  <?php _e('<p>Welcome and thank you for joining us.</p>
  <p>You may login to www.nk1000.com with your email address.<br>Start shopping and enjoy!</p>
  <p>Should you have any inquiries, please feel free to contact us at <a href="mailto:info@nipponkendai.com">info@nipponkendai.com</a>.</p>
  <p>Best regards,<br>Nippon Kendai</p>
  <p>Phone Enquiry: +852 2587 7099 / +852 2587 8313</p>'); ?>
<?php else : ?>
  <?php _e('<p>Welcome and thank you for joining us.</p>
<p>Your coupon number is 【nk0001】. You may login to <a href="http://www.nk1000.com">www.nk1000.com</a> with your email address.</p>
<p>Start shopping and enjoy your one-time discount offer!</p>
<p>Should you have any inquiries, please feel free to contact us at <a href="mailto:info@nipponkendai.com">info@nipponkendai.com</a>.</p>
<p>Best regards,<br>Nippon Kendai</p>
<p>Phone Enquiry: +852 2587 7099 / +852 2587 8313</p>'); ?>
<?php endif; ?>
<p><?php _e( "This is a post-only mailing. Replies to this message are not monitored or answered." ); ?></p>

<?php do_action( 'woocommerce_email_footer', $email );
