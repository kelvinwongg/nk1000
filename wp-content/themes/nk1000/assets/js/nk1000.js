(function( $ ){

  $(document).foundation();

  // .storefront-child-product-hero-slider START
  $(".storefront-child-product-hero-slider .slider").slick({
    adaptiveHeight: true,
    autoplay: true,
    dots: true,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear'
  });
  // .storefront-child-product-hero-slider END

  // .homepage-popup START
  $(".homepage-popup").each(function(){
    $.magnificPopup.open({
      items: {
        src: '.homepage-popup',
        type: 'inline',
      }
    });
  });
  // .homepage-popup END

  // Click to scroll JQuery
  $(".click-to-scroll").click(function() {
    $('html, body').animate({
      scrollTop: $($(this).attr("data-scrollTo")).offset().top
    }, 1000);
    return false;
  });

  // Single Product Tab START
  // $( document.body ).off( 'wc-init-tabbed-panels');
  // $( document.body ).off( 'click', 'ul.wc-tabs a' );
  // $( 'ul.wc-tabs a' ).click( function( e ) {
  //   e.preventDefault();
  //   var panel_wrap = $( this ).closest( 'div.panel-wrap' );
  //   if ($( this ).parent().hasClass('active')) {
  //     $( 'ul.wc-tabs li', panel_wrap ).removeClass( 'active' );
  //     $( this ).parent().removeClass( 'active' );
  //     $( 'div.panel', panel_wrap ).hide();
  //     $( $( this ).attr( 'href' ) ).show();
  //     alert('has active');
  //   } else {
  //     $( 'ul.wc-tabs li', panel_wrap ).removeClass( 'active' );
  //     $( this ).parent().addClass( 'active' );
  //     $( 'div.panel', panel_wrap ).hide();
  //     $( $( this ).attr( 'href' ) ).show();
  //   }
  // });
  // Single Product Tab END

  // Cart Page START
  $( document.body ).on( 'wc_fragments_refreshed', function( evt ) {
    console.log('wc_fragments_refreshed', evt);
    disable_shipping_method();
    var $calculate_user_cart_points = {
      url: '/wp-admin/admin-ajax.php',
      type: 'POST',
      data: {
        action: 'calculate_user_cart_points'
      },
      success: function( data ) {
        if ($('.gift_redeem.logged_in').length) {
          // console.log('calculate_user_cart_points', data);
          $('.total_points').html(data.points_total);
          $('.used_points').html(data.points_used_total);
          $('.remained_points').html(data.points_remain);
        }
      }
    };
    $.ajax( $calculate_user_cart_points );
  } );
  // Cart Page END

  // Cart page disable unnecessary shipping method START
  disable_shipping_method = function () {
    $('#shipping_method').each(function() {
      // console.log('disable shipping method', $(this));
      hasFreeShipping = false;
      $(this)
        .find('li input[type="radio"]')
          .each(function() {
            if (this.value.search(/free_shipping/)) hasFreeShipping = true;
          })
        .end()
        .each(function() {
          if (hasFreeShipping) {
            $(this).find('li input[type="radio"]').each(function() {
              if (this.value.search(/free_shipping/)) {
                $(this).attr('disabled', 'disabled').parent().css('opacity', '0.6');
              }
            });
          }
        })
    });
  }
  disable_shipping_method();
  // Cart page disable unnecessary shipping method END

  // Language Switcher START
  $('#lang-switch').on('change', function() {
    // console.log($(this).val());
    window.location.href = $(this).val();
  });
  // Language Switcher END

  // Open homepage popup
  $('#test-popup').each(function() {
    $.magnificPopup.open({
      items: {
        src: '#test-popup', // can be a HTML string, jQuery object, or CSS selector
        type: 'inline'
      }
    });
  });

})( jQuery );