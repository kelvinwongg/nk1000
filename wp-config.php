<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// Disable PHP SOAP CACHE
ini_set('soap.wsdl_cache_enabled',0);
ini_set('soap.wsdl_cache_ttl',0);

// Increase PHP Memory Limit for WPML
define('WP_MEMORY_LIMIT', '128M');
define( 'WP_MAX_MEMORY_LIMIT', '128M' );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nk1000');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

// WordPress URL
define('WP_SITEURL', 'http://nk1000.local');
define('WP_HOME',    'http://nk1000.local');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ESHt|6g+Z#S)?&[}>ne)D^oNaQHaK{i.&ogIR M:?e#6pN4e,vk&{}qt3H!+Se~q');
define('SECURE_AUTH_KEY',  'eCE/1p4uD@h(nFf1q$gAg~DOF<6#(mKUL!/5lLCh<g.g1j{r)[F{W|e :zoB- Ky');
define('LOGGED_IN_KEY',    '0VF#yeDx]C$1f~vgdz[w),sAhLT}Al|Ln}!g^.Ax.2|?1yn4P0OF)NbS)j5;y|tU');
define('NONCE_KEY',        '^(C(g#s.[re}:/FlH;u4eSsgT[>Ur@.<_JJZPrvGEKP?@-|/$6E+)YAwmk.%pARJ');
define('AUTH_SALT',        'SGPD@6Y?lRb0=yYwlBQ#Po.=Uj?b,]0VS4rC(ddtVXG~kg0|B}~a43 ~Y*9@N6{_');
define('SECURE_AUTH_SALT', '*8}lprpN]xX}iKb.QRrT~~dr&B ]EI>={!xS=!8czkouP8nu,g&<WPkc!4FhYCq:');
define('LOGGED_IN_SALT',   '>FZxki`S{o!J:F~)&J>iBF/E}-TzUlu.-)IS;b,PlvW)akqcuEBAr5M&;~$[Z7_p');
define('NONCE_SALT',       'N%n@Z8S/D0[G{hnh*;7D mO=<Uea}=H}}xtuZjFq7Wx:xZ;($@z?IMeOZwgYDfpj');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
